##### General control settings ######

#Thread Count: The number of threads to use for parallel processing
THREAD_COUNT = 1

###### Settings describing the input probe dataset ######

#Time stamp field: Field name containing timestamps
TIMESTAMPFIELD = 'sample_date' 

#Probe field: Field name containing unique probe IDs
PROBEFIELD =  'probe_id' 

#Provider field: Field name containing unique data provider IDs
PROVIDERFIELD = 'probe_data_provider' 

#Lat field: Field name containing latitude data
LATFIELD = 'lat' 

#Lon field: Field name containing longitude data
LONFIELD = 'lon'

#Timestamp Precision: The measurement precision of the timestamps on the data
TIMESTAMP_PRECISION = 1. 

#Timestamp Keywords: for column headers containing these substrings, the code will
#try to treat them as timestamps
TIMESTAMP_KEYWORDS = ['time', 'date']


###### Settings for splitting traces on long time lags ######
#Lag factor: The minimum number of missed samples required to split a trace on a lag
LAG_FACTOR = 10

#Minimum lag: The absolute minimum duration (in seconds) required to split a trace on a gap.
MIN_LAG = 600 


###### Settings for stationary point identification ######
#Angle threshold: The threshold in integrated two-point heading change for identifying potential stationary points
ANGLE_THRESHOLD = 155 

#Error radius: The radius (in km) within which a point is considered stationary. If this is expanded this function can be used for stay point detection.
ERROR_RADIUS = .016

#Two sides: If True, points on both sides of the candidate (previous and subsequent) must be within the error radius 
# to find a stationary point; otherwise only the previous point. Setting to false will cause an uptick in points found but increase false positives
TWO_SIDES = True 

#Minimum points: The minimum number of points to identify a stationary point
MIN_PTS = 3 

#Minimum duration:The minimum duration to identify a stationary point
MIN_DUR = 30 

###### Settings for error detection ######
#Anomalous fraction threshold: The fraction of anomalous points in a trace required to flag the trace as having an excessive anomaly fraction
ANOM_FRAC_THRESH = 0.1 

#Velocity threshold: The threshold to flag velocity outliers (in kph) 
VELOCITY_THRESH = 160 

#Heading Change threshold: The minimum heading change over two points to flag that point as a heading outlier. 
HEADING_CHANGE_THRESH = 270 

#Split Irregular Sample Periods: If a trace does not have a regular sampling period, split based on the minimum lag
SPLIT_IR_SAMP_RATE = False