#Import the default settings first
from .defaults import *

#Override the defaults with any local settings
try:
    from .local import *
except ImportError:
    pass
try:
    TIMESTAMPFIELD = TIMESTAMPFIELD.lower()
except:
    pass
try:
    PROBEFIELD = PROBEFIELD.lower()
except:
    pass
try:
    PROVIDERFIELD = PROVIDERFIELD.lower()
except:
    pass
try:
    LATFIELD = LATFIELD.lower()
except:
    pass
try:
    LONFIELD = LONFIELD.lower()
except:
    pass
try:
    TIMESTEPFIELD = TIMESTEPFIELD.lower()
except:
    pass