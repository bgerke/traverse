
from .identify import(create_unique_trace_ids)
import numpy as np
from traverse.settings import *

def get_mode(trace, timestepfield='sec_since_prev'):
    '''
   This function takes a gps trace and return the sampling rate.
   It assumes that the lowest modal frequency is the intended sampling rate.

   Inputs
   ---
   trace-- the gps trace that you wish to analyze

    timestepfield-- the seconds between each gps reading

    ----
     '''
    modes = trace[timestepfield].mode()
    modes = modes[modes > 0]
    if len(modes) == 0:
        mode = -1
    else:
        mode = min(modes)
    return mode


def get_sampling_period(trace, rate_thresh=.5, timestepfield='sec_since_prev',
                      timestamp_precision=1, calculate_mode=True, mode=None):
    '''
    This function takes a gps trace and calculates the sampling rate.
    The sampling rate is assumed as  the lowest mode seconds between samples.
    If the proportion of this mode is 50% or a combination of its factors
    is over 50% then it is listed as the sampling rtate.
    It returns the trace with an added column sample_period.
    If a regular sampling rate exists it will be returned.
    Otherwise the sampling rate will be NaN

    INPUTS
    --------

    trace-- a gps trace

    rate_thresh-- the threshold at which a rate is considered the sampling rate

    timestepfield-- the seconds between each sample

    timestamp_precision-- the timestamp precision. Used as allowable error.

    calculate_mode-- calculate the mode. Default is True. If set to False
        user must pass in mode

    mode-- mode to pass in if get_mode is false.

    '''

    # grab the mode
    if calculate_mode:
        mode = get_mode(trace)
    samprate = np.nan
    single_samprate = False
    # get the value counts for the sampling rate
    vals = trace[timestepfield].value_counts(normalize=True).reset_index()
    prop = vals.loc[vals['index'] == mode, timestepfield].iloc[0]
    val = vals.loc[vals['index'] != mode].set_index('index')
    for ind in val.index:
        # if the proportion of value counts is above the threshold set the mode
        # as the sampling rate
        if prop >= rate_thresh:
            # Will get set by fall through logic
            break
        # if the frequency of the mode was not a majority of the trace,
        # see whether including integer multiples of the mode (plus or minus
        # the timestamp precision) yields a majority.
        if (ind % mode <= timestamp_precision) | (
                abs(ind % mode - mode) <= timestamp_precision):
            # include the multiples in the next proportion check
            prop = prop + val.loc[ind, timestepfield]
    # check last iteration
    if prop >= rate_thresh:
        # the mode is the sampling rate if the criteria are met.
        samprate = mode
        single_samprate = True
    # if the mode was not the sampling rate,
    if ~single_samprate:
        # find any other potential candidates (equal number of occurances as
        # the mode)
        ties = vals.loc[vals[timestepfield] == vals.loc[vals['index'] == mode, 
                        timestepfield].iloc[0]].reset_index()
        # get a data frame of potential candidates
        ties = ties.loc[ties['index'] != mode]
        if len(ties) > 0:
            for tie in ties['index']:
                val = ties.loc[ties['index'] != tie].set_index('index')
                prop = ties.loc[ties['index'] == tie, timestepfield].iloc[0]
                if (single_samprate) | (abs(tie) == 1):
                    break
                # Repeat the logic above to determine whether this is the
                # sampling rate.
                for ind in val.index:
                    if prop >= rate_thresh:
                        # same logic as above
                        break
                    if (ind % tie <= timestamp_precision) | (
                            abs(ind % tie - tie) <= timestamp_precision):
                        prop = prop + val.loc[ind, timestepfield]
                # check last combination
                if prop >= rate_thresh:
                    samprate = tie
                    single_samprate = True
    trace['sample_period'] = samprate
    return trace


def detect_large_lag(data,
                     lag_factor, min_lag, 
                     split_ir_samp_rate, timestepfield='sec_since_prev' ):
    '''
    Detect any  large lags. When a gps trace loses signal for a prolonged
    period it may be bet to split a single trace into two. At the very
    least it is important to detect these long pauses in data reporting as
    they are often connected to errors.

    Returns the input dataframe with a new column, 'long_lag_flag' that
    flags all datapoints that were preceded by an excessively long lag
    (as defined by the input parameters).

    The lag_factor and min_lag parameters determine the thresholds for
    defining a large lag: the time step between two data points must be longer
    than both some absolute duration (min_lag), as well as a specified
    multiple of the sampling rate (lag_factor). For instance, we might
    specify that a lag must be at least 60 seconds long AND at least 10x the
    nominal sampling rate for a trace, in order to be flagged as excessively
    long.

    Arguments
    ---------
    data-- gps trace data to detect large lags on

    Keyword arguments
    -----------------
    timestepfield: the field with seconds from previous
    lag_factor: the multiple of a trace's sampling rate at which a
        point should be flagged as having been preceded by a long lag.
    min_lag: the minimum absolute duration
    split_ir_samp_rate: If True, split traces with irregular sampling rates 
        based on min_lag.


    '''
    data['long_lag_flag'] = 0
    large_lag = ((data['sample_period'] * lag_factor < data[timestepfield]) &
                 (data[timestepfield] > min_lag))
    data.loc[large_lag, 'long_lag_flag'] = 1
    #if one wants to split traces without sampling rates
    if split_ir_samp_rate:
        large_lag_nsr = ((data['sample_period'].isna())&
                         (data[timestepfield] > min_lag))
        data.loc[large_lag_nsr, 'long_lag_flag'] = 1
    return data


def reflag_large_lag(trace, tracefield):
    '''
    Reflag any large lags by trace. This function gives unique ids to each 
    series of large lags so that they can be easily split on later. It also 
    marks newly flagged traces to be split using the timestepfield.

    INPUTS
    ------

    trace-- the gps trace to be flagged with points to split on

    tracefield-- the field containing unique ids for each trace

    '''
    # get the index of where to split
    index_split = trace.loc[trace['long_lag_flag'] == 1].index
    indstart = trace.index[0]
    probe = trace[tracefield].unique()[0]
    trace['split_field'] = probe
    run = 1
    for ind in index_split:
        # give a common flag to split up the traces on
        trace.loc[indstart:ind - 1, 'split_field'] = str(probe) + str(run)
        run = run + 1
        indstart = ind
    return trace


def split_on_large_lag(data, timestepfield='sec_since_prev',
                       lag_factor=LAG_FACTOR, min_lag=MIN_LAG, 
                       tracefield='trace_id', 
                       crowflies=True,
                       split_ir_samp_rate = SPLIT_IR_SAMP_RATE, **kwargs):
    '''
    Split every trace based on the large lags detected in sampling rate.
    Recode the values for the timestepfield to negative 1 for the first point 
    in new traces The data that are split up will be returned with the sampling 
    rate field set to indicate the first point in a new trace. Data should be 
    preprocessed using crowflies. If wishing to use data unprocessed by 
    crowflies, set crowflies as false. If true the standard naming scheme will 
    be assumed. If not only the timestepfield will be reset. The dataset must 
    be run through the unique id generator prior to running this function.
    It is highly recommended that one preprocesses with crowflies.

    INPUTS
    -----

    data -- the gps trace data to process

    KEYWORD INPUTS
    --------------

    timestepfield: field containing the seconds between each sample
    lag_factor: the multiple of a trace's sampling rate at which a point 
        should be thrown out
    min_lag: the minimum duration for a laggy point to be tossed
    tracefield: the field containing a unique ID for each trace
    crowflies: indicates that data has been processed through crowflies.
    split_ir_samp_rate: If True, split traces with irregular sampling rates 
        based on min_lag.
    **kwargs: Additional keywords passed through to get_sampling_period 

    '''
    # detect the large lags
    data = detect_large_lag(data, lag_factor, min_lag, split_ir_samp_rate, timestepfield)
    # get the list of traces that must be split
    traces = data.loc[data['long_lag_flag'] == 1, tracefield].unique()
    # create a new data frame subset of what to split up
    dat = data.loc[data[tracefield].isin(traces)]
    if len(dat)==0:
        #Nothing to split. Quit.
        return data
    #reset_index
    dat.reset_index(inplace=True)
    # reflag to make split possible
    dat = dat.groupby(
        tracefield,
        as_index=False).apply(
        reflag_large_lag,
    tracefield=tracefield)
    #If we got any splits...
    # create new ids based on the new splits
    dat = create_unique_trace_ids(
        dat, probefield='split_field', providerfield=False)
    dat.drop(['split_field'], axis=1, inplace=True)
    #set cross trace calculations to zero
    #backward (probe is the same as previous)
    match_probe_b = dat['trace_id'].eq(dat['trace_id'].shift())
    #forward (probe is the same as subsequent)
    match_probe_f = dat['trace_id'].eq(dat['trace_id'].shift(periods=-1))
    dat.loc[~match_probe_b, timestepfield]=-1
    #Make sure the second point in each trace is also zero for abs_headch
    if crowflies:
        dat.loc[~match_probe_f, 'simple_head_ch_from_prev'] = 0
        dat.loc[~match_probe_f, 'abs_heading_change_last_2'] = 0
        dat.loc[~match_probe_b, 'dist_from_prev'] = 0
        dat.loc[~match_probe_b, 'avg_vel_from_prev'] = 0
        dat.loc[~match_probe_b, 'min_avg_vel_from_prev'] = 0
        dat.loc[~match_probe_b, 'heading_from_prev'] = 0
        dat.loc[~match_probe_b, 'simple_head_ch_from_prev'] = 0
        dat.loc[~match_probe_b, 'abs_heading_change_last_2'] = 0
        match_probe_2 = dat['trace_id'].eq(dat['trace_id'].shift(periods=2))
        dat.loc[~match_probe_2, 'abs_heading_change_last_2']=0
    #put the index back
    dat.set_index(list(dat)[0], inplace=True)
    #recalculate the sampling rates
    dat = dat.groupby('trace_id').apply(
        get_sampling_period, timestepfield=timestepfield,**kwargs)
    # put the data back in
    data.loc[dat.index] = dat
    data.drop(columns=['long_lag_flag'], axis=1, inplace=True)
    return data