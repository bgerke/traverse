import pandas as pd
import numpy as np
from ..utils.coords import great_circ_km, great_circ_head
from traverse.settings import *


def find_stationary_points(
        data,
        angle_threshold=ANGLE_THRESHOLD,
        error_radius=ERROR_RADIUS,
        two_sides=TWO_SIDES,
        min_pts=MIN_PTS,
        min_dur=MIN_DUR,
        latfield=LATFIELD,
        lonfield= LONFIELD,
        is_sorted = True,
        timestampfield=TIMESTAMPFIELD):
    '''
    This function identifies points with large heading changes and small position changes.
    If there are large heading changes and small position changes the vehicle is likely at rest.
    We use a single erroneous heading change to identify potential stay points and expand the
    identifications to all points that are temporally connected to the stay point without leaving the error radius 
   (i.e. the sub-trace surrounding a given stay point that is inside of the error radius). Preprocessing through 
   crowflies is assumed. This implies that each trace is sorted by time. 

    Inputs
    ------

    data - the entire data set of gps traces to be analyzed. Must have heading change calculated

    Keywords
    ---------

    angle_threshold - the threshold at which to flag a suspicious head change over the last two points

    error_radius - the maximum radius that a point remains a stay point. This is calculated from the
    first detected point.

    two_sides - a boolean. If true stationary detection requires both sides of a heading change to be within the error radius. If false only one side is required

    min_pts - the minimum allowable points in a stationary point

    min_dur - the minimum required duration of a trace within a radius to be labled a stationary point
        
    is_sorted - If true, then the input trace is sorted in ascending
    order of timestamp, so we can skip sorting it internally.
    
    timestampfield - the time stamp column to sort by

    '''
    # Ensure the index is continuous
    data = data.reset_index()
    if not is_sorted:
        data.sort_values(by =['trace_id',timestampfield], inplace=True)
    data_shift = data.shift(periods=-1)
    # Generate the key to seperate the traces while processing
    match_probe = data.trace_id.eq(data.trace_id.shift())
    # get the index of all stay points
    # If a point on either side of the point with irregular head change
    # is within the stationary radius, mark it as a stationary point
    if two_sides:
        # use an and to require both sides
        stay_index = data.loc[(match_probe) & ((data['abs_heading_change_last_2'] > angle_threshold) & (data['dist_from_prev'] < error_radius)) &
                              ((data['abs_heading_change_last_2'] > angle_threshold) & (data_shift['dist_from_prev'] < error_radius))].index
    else:
        # use an or to require only one side
        stay_index = data.loc[(match_probe) & ((data['abs_heading_change_last_2'] > angle_threshold) & (data['dist_from_prev'] < error_radius)) |
                              ((data['abs_heading_change_last_2'] > angle_threshold) & (data_shift['dist_from_prev'] < error_radius))].index
    # Execute stationary point flagging
    # put in the flag label for all traces
    data['stationary_point_flag'] = -1
    # only send data send the data that actually has a stationary point flag
    stay_probes = data.loc[stay_index, 'trace_id'].unique()
    stay_data = data.loc[data['trace_id'].isin(stay_probes)]
    stay_data = stay_data.groupby(
        'trace_id',
        as_index=False).apply(
        stationary_loop,
        stay_index=stay_index,
        error_radius=error_radius,
        min_pts=min_pts,
        min_dur=min_dur,
        latfield=latfield,
        lonfield=lonfield,
        timestampfield=timestampfield)
    # Put the processed data back into the data frame
    data.loc[stay_data.index,
             'stationary_point_flag'] = stay_data['stationary_point_flag']
    # drop the column we no longer need and set the index back to original
    data = data.set_index(list(data)[0])
    return data


def stationary_loop(trace, stay_index, error_radius, min_pts, min_dur,latfield,lonfield,timestampfield):
    '''
    This loop does the bulk of the work for the stationary point detection. All parameters are passed through
    by the stationary point function. The index is continuous sorted by time. The values in the stay_index are
    a list of the potential stationary points. Not intended for use outside of find_stationary_points

    Inputs
    ------

    trace - the trace to be analyzed.

    error_radius - the maximum radius that a point remains a stay point. This is calculated from the
    first detected point.

    two_sides - a boolean. If true stationary detection requires both sides of a heading change to be within the error radius. If false only one side is required

    min_pts - the minimum allowable points in a stationary point

    min_dur - the minimum required duration of a trace within a radius to be labled a stationary point

    stay_index - the index of potential stationary points
    '''
    flag = 1
    trace['dist_from_stay'] = 10 ^ 3
    # trims the index to the given trace
    stay_index = stay_index[np.isin(stay_index, trace.index)]
    for i in stay_index:
        # if this is not a part of an already identified stay point
        if trace.loc[i, 'stationary_point_flag'] == -1:
            # calculate the distance from the stay point to other points in the
            # trace
            trace['dist_from_stay'] = \
                great_circ_km(
                    trace.loc[i, latfield], trace.loc[i, lonfield],
                    trace[latfield], trace[lonfield]
            )
            # get the indices for points that qualify to be flagged
            new_stay_index = trace.loc[(trace['dist_from_stay'] < error_radius) & (
                trace['stationary_point_flag'] == -1)].index
            # if it is continuous, given the continuous index sorted by time,
            # flag all points
            if (new_stay_index.max() - new_stay_index.min() +
                    1) == len(new_stay_index):
                # check the requirements of points and duration are met
                if (len(new_stay_index) > min_pts - 1) & (calc_dur(trace.loc[new_stay_index.min():new_stay_index.max(), timestampfield], min_dur)):
                    trace.loc[new_stay_index, 'stationary_point_flag'] = flag
                    # for unique flags for each trace use
                    #str(trace.loc[cont_range[0]:cont_range[1]].trace_num.unique()) + '_' +  str(flag)
                    flag = flag + 1
                # if it is not continuous, find the adjacent points to the stay
                # point
            else:
                # create a list of the continuous ranges
                cont_list = continuity_chop(new_stay_index)
                # find the appropriate range that contains the stay point and flag
                for cont_range in cont_list:
                        # if the stay point is in the range
                    if i in range(cont_range[0], cont_range[1]):
                        if (cont_range[1] - cont_range[0] > min_pts - 1) & (calc_dur(trace.loc[cont_range[0]:cont_range[1], timestampfield], min_dur)):
                            # flag all appropriate continuous points, then
                            # break
                            trace.loc[cont_range[0]:cont_range[1],
                                      'stationary_point_flag'] = flag
                            # for unique flags for each trace use
                            #str(trace.loc[cont_range[0]:cont_range[1]].trace_num.unique()) + '_' +  str(flag)
                            flag = flag + 1
                            break
    trace = trace.drop(columns='dist_from_stay')
    return trace


def calc_dur(times, min_dur):
    '''
    This function calculates the duration between two points.
    It checks if that duration is less than a given minimum threshold.

    INPUTS
    ------
    times - a list of type datetime 

    min_dur - the duration to measure against. Measured in seconds
    '''
    dur = times.max() - times.min()
    dur = dur / np.timedelta64(1, 's')
    return dur > min_dur


def continuity_chop(indx):
    '''
    This function takes an index (a numpy array) and chops it up into continuous ranges
    The function returns a list of tuples containing the start and end index
    For each continuous section in the index

    INPUTS
    -------

    indx -- the sequence to divide by continuity. Sequence must be a numpy array. '''

    cont_list = []
    k = 0
    while k < len(indx)-1:
        start = indx[k]
        while (indx[k+1] == indx[k]+1) & (k < len(indx)-2):
            k = k+1
        end = indx[k]
        if (k == len(indx)-2) & (indx[k+1] == indx[k]+1):
            end = indx[k+1]
        k = k+1
        cont_list.append((start, end))
    return cont_list
