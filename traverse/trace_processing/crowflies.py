import pandas as pd
import numpy as np

from ..utils.coords import great_circ_km, great_circ_head
from traverse.settings import *

def compute_time_dist_avg_vel(trace, 
                              timestampfield=TIMESTAMPFIELD,
                              latfield=LATFIELD,
                              lonfield=LONFIELD,
                              timestamp_precision = 1., 
                              is_sorted=False):
    '''
    For a given trace (group with a single probe ID), compute
     * the time since the previous sample
     * the distance from the previous sample (great circle)
     * the average velocity magnitude implied by the above.
     * the heading from the previous location
     
     Inputs
     -------
     trace - A dataframe (or group object) that contains a timestamp,
     latitude, and longitute field for at least two points.
     
     Keywords
     --------
     timestampfield, latfield, lonfield - Names of the timestamp, latitute
         and longitude column headers in the input trace.

     timestamp_precision: the reporting precision of the timestamp field
         (in seconds). If the timestamps are reported to millisecond 
         precision, set this to 0.001. Default is 1 second.
     
     is_sorted: If true, then the input trace is sorted in ascending
         order of timestamp, so we can skip sorting it internally.

     Returns the input trace dataframe, augmented with additional columns
     named 'sec_since_prev', 'dist_from_prev', 'avg_vel_from_prev', and
     'min_avg_vel_from_prev'
    '''
    
    if not is_sorted:
        trace.sort_values(timestampfield, inplace=True)

    shifted = trace[[timestampfield,latfield,lonfield]].shift()

    trace['sec_since_prev'] = \
        trace[timestampfield].sub(
            shifted[timestampfield]).fillna(-1).apply(
                pd.Timedelta.total_seconds
                )
    trace['dist_from_prev'] = \
        great_circ_km(
            shifted[latfield], shifted[lonfield],
            trace[latfield], trace[lonfield]
            )

    trace['avg_vel_from_prev']= \
        (trace['dist_from_prev']/
         trace['sec_since_prev']*3600).fillna(0)

    #Compute the *minimum* value that the average velocity
    #could take, given the 1-second timestamp precision.
    trace['min_avg_vel_from_prev'] = \
        (trace['dist_from_prev']/
         (timestamp_precision+trace['sec_since_prev'])*3600).fillna(0)
        
    trace['heading_from_prev'] = \
        great_circ_head(
                shifted[latfield], shifted[lonfield],
                trace[latfield], trace[lonfield]
                ).fillna(0)
    
    return trace

def compute_time_dist_avg_vel_all(data, 
                              timestampfield=TIMESTAMPFIELD,
                              tracefield = 'trace_id',
                              latfield=LATFIELD,
                              lonfield=LONFIELD,
                              timestamp_precision = TIMESTAMP_PRECISION, 
                              is_sorted=False):
    '''
    For the entire data set compute
     * the time since the previous sample
     * the distance from the previous sample (great circle)
     * the average velocity magnitude implied by the above.
     * the heading from the previous location
     
     Inputs
     -------
     data - A dataframe (or group object) that contains a timestamp,
     latitude, and longitute field for at least two points.
     
     Keywords
     --------
     timestampfield, latfield, lonfield - Names of the timestamp, latitute
         and longitude column headers in the input trace.

     timestamp_precision: the reporting precision of the timestamp field
         (in seconds). If the timestamps are reported to millisecond 
         precision, set this to 0.001. Default is 1 second.
     
     is_sorted: If true, then the input trace is sorted in ascending
         order of timestamp, so we can skip sorting it internally.

     Returns the input trace dataframe, augmented with additional columns
     named 'sec_since_prev', 'dist_from_prev', 'avg_vel_from_prev', and
     'min_avg_vel_from_prev'
    '''
    
    if not is_sorted:
        data.sort_values(by =[tracefield,timestampfield], inplace=True)

    shifted = data[[timestampfield,latfield,lonfield]].shift()
    match_probe = data.trace_id.eq(data.trace_id.shift())

    data.loc[:,'sec_since_prev'] = \
        data[timestampfield].sub(
            shifted[timestampfield]).fillna(-1).apply(
                pd.Timedelta.total_seconds
                )
    data.loc[:,'dist_from_prev'] = \
        great_circ_km(
            shifted[latfield], shifted[lonfield],
            data[latfield], data[lonfield]
            )

    data.loc[:,'avg_vel_from_prev']= \
        (data['dist_from_prev']/
         data['sec_since_prev']*3600).fillna(0)

    #Compute the *minimum* value that the average velocity
    #could take, given the 1-second timestamp precision.
    data.loc[:,'min_avg_vel_from_prev'] = \
        (data['dist_from_prev']/
         (timestamp_precision+data['sec_since_prev'])*3600).fillna(0)
        
    data.loc[:,'heading_from_prev'] = \
        great_circ_head(
                shifted[latfield], shifted[lonfield],
                data[latfield], data[lonfield]
                ).fillna(0)
    
    data.loc[ ~match_probe,'sec_since_prev'] = -1
    data.loc[ ~match_probe,'dist_from_prev'] = 0
    data.loc[ ~match_probe,'avg_vel_from_prev'] = 0
    data.loc[ ~match_probe,'min_avg_vel_from_prev'] = 0
    data.loc[ ~match_probe,'heading_from_prev'] = 0
    return data


def compute_simple_head_ch(trace, timestampfield=TIMESTAMPFIELD, 
                           headingfield ='heading_from_prev',
                           is_sorted = False):
    '''
    Calculates the difference in heading between the current and previous point
    and returns the value as a new column
    
    Inputs
     -------
     trace - the trace to be analyzed
     
     is_sorted: If true, then the input trace is sorted in ascending
     order of timestamp, so we can skip sorting it internally.     
    '''
    
    if not is_sorted:
        trace.sort_values(timestampfield, inplace=True)
        
    trace['simple_head_ch_from_prev'] = trace[headingfield].diff(periods=-1)*-1
    trace.loc[0,'simple_head_ch_from_prev']=0
    trace.loc[len(trace)-1,'simple_head_ch_from_prev']=0
    
    return trace
    
def compute_abs_head_ch(trace, 
                        timestampfield='sample_date', is_sorted = False):
    '''
    Computes the integrated absolute heading change between the previous
    two points
    
    Inputs
     -------
     trace - the trace to be analyzed
     
     is_sorted: If true, then the input trace is sorted in ascending
     order of timestamp, so we can skip sorting it internally.    
    
    '''
    if not is_sorted:
        trace.sort_values(timestampfield, inplace=True)
    
    #compute simple head change
    trace = compute_simple_head_ch(trace, is_sorted = True)
    #take the absolute value and normalize between 0-180
    abs_next = trace['simple_head_ch_from_prev'].apply(abs).apply(
        lambda x: min(x,360-x))
    #perform the same operation for the next heading change
    abs_prev = trace['simple_head_ch_from_prev'].shift().apply(abs).apply(
        lambda x: min(x,360-x))
    #combine to get total heading change
    trace.loc[:,'abs_heading_change_last_2'] = abs_prev+abs_next
    trace.loc[len(trace)-1,'abs_heading_change_last_2'] = 0
    trace.loc[0,'abs_heading_change_last_2'] = 0
    
    return trace


def data_set_calc_head_ch(data, _sorted=False):
    '''
    Takes a data set and calculates heading change for each trace
    
    Inputs
    ------
    
    data- a dataset of gps traces    
        
    '''       
    data = data.groupby('trace_id').apply(compute_abs_head_ch, 
                                          is_sorted=_sorted)
    #put in any flagging here
    return data


#Here are the equations to do head_ch all at once
def compute_simple_head_ch_all(data, timestampfield=TIMESTAMPFIELD, 
                               tracefield = 'trace_id',
                               headingfield = 'heading_from_prev', 
                               is_sorted = False):
    '''
    Calculates the difference in heading between the current and 
    previous point and returns the value as a new column
    
    Inputs
     -------
     data - the gps data to be analyzed. Must have a headings
     
     is_sorted: If true, then the input trace is sorted in ascending
     order of timestamp, so we can skip sorting it internally.     
    '''
    
    if not is_sorted:
        data.sort_values([tracefield,timestampfield] , inplace=True)
    
    #Get the headings and make them all positive
    data.loc[:,'simple_head_ch_from_prev'] = \
        data[headingfield].diff(periods=-1)*-1
    #set the first and last points to zero
    data = data.reset_index()
    data.loc[0,'simple_head_ch_from_prev']=0
    data.loc[len(data)-1,'simple_head_ch_from_prev']=0
    data = data.set_index('index')
    #set cross trace calculations to zero
    #backward (probe is the same as previous)
    match_probe_b = data[tracefield].eq(data[tracefield].shift())
    #forward (probe is the same as subsequent)
    match_probe_f = data[tracefield].eq(data[tracefield].shift(periods=-1))
    data.loc[~match_probe_f, 'simple_head_ch_from_prev']=0
    data.loc[~match_probe_b, 'simple_head_ch_from_prev']=0
    
    return data
    
def compute_abs_head_ch_all(data, 
                            timestampfield=TIMESTAMPFIELD, 
                            tracefield = 'trace_id', 
                            headingfield = 'heading_from_prev',
                            is_sorted = False):
    '''
    Computes the integrated absolute heading change between the previous
    two points
    
    Inputs
     -------
     data - the data to be analyzed
     
     is_sorted: If true, then the input trace is sorted in ascending
     order of timestamp, so we can skip sorting it internally.    
    
    '''
    if not is_sorted:
        data.sort_values([tracefield,timestampfield], inplace=True)
    
    #compute simple head change
    data = compute_simple_head_ch_all(data, is_sorted = True)
    #take the absolute value and normalize between 0-180
    abs_next = data['simple_head_ch_from_prev'].apply(abs).apply(
        lambda x: min(x,360-x))
    #perform the same operation for the next heading change
    abs_prev = data['simple_head_ch_from_prev'].shift().apply(abs).apply(
        lambda x: min(x,360-x))
    #combine to get total heading change
    data.loc[:,'abs_heading_change_last_2'] = abs_prev+abs_next
    #set the first and last points to zero
    data = data.reset_index()
    data.loc[len(data)-1,'abs_heading_change_last_2'] = 0
    data.loc[0,'abs_heading_change_last_2'] = 0
    #set the second point zero (it takes three to calculate this)
    data.loc[1,'abs_heading_change_last_2'] = 0
    data =data.set_index('index')
    
    #set cross trace calculations to zero
    #backward (probe is the same as previous)
    match_probe_b = data[tracefield].eq(data[tracefield].shift())
    #forward (probe is the same as subsequent)
    match_probe_f = data[tracefield].eq(data[tracefield].shift(periods=-1))
    data.loc[~match_probe_f, 'abs_heading_change_last_2']=0
    data.loc[~match_probe_b, 'abs_heading_change_last_2']=0
    #Make sure the second point in each trace is also zero
    match_probe_2 = data[tracefield].eq(data[tracefield].shift(periods=2))
    data.loc[~match_probe_2, 'abs_heading_change_last_2']=0
    
    return data

