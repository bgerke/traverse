import pandas as pd
import numpy as np
from ..crowflies import (compute_time_dist_avg_vel, compute_abs_head_ch, 
                         compute_simple_head_ch, compute_time_dist_avg_vel_all, 
                         compute_simple_head_ch_all, compute_abs_head_ch_all)
from ...utils.coords import great_circ_km, great_circ_head

def test_compute_time_dist_avg_vel_uses_gc_funcs():
    '''
    The compute_time_dist_avg_vel function should give the results
    as calculated by the great-circle functions
    '''
    #First create a test trace made up of 10 random points in a 
    #1-degree square
    npts = 10
    dates = pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats = np.random.rand(10)+np.random.rand(1)[0]*89*2-89
    lons = np.random.rand(10)+np.random.rand(1)[0]*179*2-179
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    

    test_trace = compute_time_dist_avg_vel(test_trace)

    for i in range(1,npts):
        assert abs(test_trace.loc[i,'sec_since_prev'] -
            (test_trace.loc[i, 'sample_date'] - 
             test_trace.loc[i-1,'sample_date'])/np.timedelta64(1,'s'))<1.e-9

        assert abs(test_trace.loc[i,'dist_from_prev'] - 
                   great_circ_km(test_trace.loc[i-1, 'lat'], 
                                 test_trace.loc[i-1, 'lon'],
                                 test_trace.loc[i,'lat'],
                                 test_trace.loc[i,'lon'])) < 1.e-9

        assert abs(test_trace.loc[i,'heading_from_prev'] -
            great_circ_head(test_trace.loc[i-1, 'lat'], 
                          test_trace.loc[i-1, 'lon'],
                          test_trace.loc[i,'lat'],
                          test_trace.loc[i,'lon']))<1.e-9

    assert (test_trace.loc[1:, 'avg_vel_from_prev'] == \
            test_trace.loc[1:, 'dist_from_prev']/
            test_trace.loc[1:, 'sec_since_prev']*3600).all()
    
    assert (test_trace.loc[1:, 'min_avg_vel_from_prev'] == \
            test_trace.loc[1:, 'dist_from_prev']/
            (1+test_trace.loc[1:, 'sec_since_prev'])*3600).all()

        

def test_compute_time_dist_avg_vel_can_take_alternate_cols():
    '''
    The compute_time_dist_avg_vel function can accept alternate
    column names
    '''
    #First create a test trace made up of 10 random points in a 
    #1-degree square
    npts = 10
    dates = pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats = np.random.rand(npts)+np.random.rand(1)[0]*89*2-89
    lons = np.random.rand(npts)+np.random.rand(1)[0]*179*2-179
    test_trace = pd.DataFrame({'timestamp':dates,'latitude':lats,
                               'longitude':lons})
    

    test_trace = compute_time_dist_avg_vel(test_trace, 
                                           timestampfield='timestamp',
                                           latfield='latitude',
                                           lonfield='longitude')

    for i in range(1,npts):
        assert abs(test_trace.loc[i,'sec_since_prev'] - \
            (test_trace.loc[i, 'timestamp'] - 
             test_trace.loc[i-1,'timestamp'])/np.timedelta64(1,'s')) < 1.e-9

        assert abs(test_trace.loc[i,'dist_from_prev'] - \
            great_circ_km(test_trace.loc[i-1, 'latitude'], 
                          test_trace.loc[i-1, 'longitude'],
                          test_trace.loc[i,'latitude'],
                          test_trace.loc[i,'longitude'])) < 1.e-9

                        
def test_min_avg_vel_uses_sampling_precision():
    '''
    The compute_time_dist_avg_vel function should use the provided
    timestamp sampling precision to compute the minimum avg velocity.
    '''
    #First create a test trace made up of 10 random points in a 
    #1-degree square
    npts = 10
    dates = pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats = np.random.rand(10)+np.random.rand(1)[0]*89*2-89
    lons = np.random.rand(10)+np.random.rand(1)[0]*179*2-179
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    

    test_trace = compute_time_dist_avg_vel(test_trace, 
                                           timestamp_precision=0.2)   

    
    assert (test_trace.loc[1:, 'min_avg_vel_from_prev'] == \
            test_trace.loc[1:, 'dist_from_prev']/
            (0.2+test_trace.loc[1:, 'sec_since_prev'])*3600).all()
    
    
def test_compute_time_dist_avg_vel_all_uses_gc_funcs():
    '''
    The compute_time_dist_avg_vel_all function should give the results
    as calculated by the great-circle functions
    '''
    #First create a test trace made up of 10 random points in a 
    #1-degree square
    npts = 10
    dates = pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats = np.random.rand(10)+np.random.rand(1)[0]*89*2-89
    lons = np.random.rand(10)+np.random.rand(1)[0]*179*2-179
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    

    test_trace = compute_time_dist_avg_vel_all(test_trace)

    for i in range(1,npts):
        assert abs(test_trace.loc[i,'sec_since_prev'] - \
            (test_trace.loc[i, 'sample_date'] - 
             test_trace.loc[i-1,'sample_date'])/np.timedelta64(1,'s')) < 1.e-9

        assert abs(test_trace.loc[i,'dist_from_prev'] - \
            great_circ_km(test_trace.loc[i-1, 'lat'], 
                          test_trace.loc[i-1, 'lon'],
                          test_trace.loc[i,'lat'],
                          test_trace.loc[i,'lon'])) < 1.e-9

        assert abs(test_trace.loc[i,'heading_from_prev'] - \
            great_circ_head(test_trace.loc[i-1, 'lat'], 
                          test_trace.loc[i-1, 'lon'],
                          test_trace.loc[i,'lat'],
                          test_trace.loc[i,'lon'])) < 1.e-9

    assert ((test_trace.loc[1:, 'avg_vel_from_prev'] - \
            test_trace.loc[1:, 'dist_from_prev']/
            test_trace.loc[1:, 'sec_since_prev']*3600).abs()<1.e-9).all()
    
    assert ((test_trace.loc[1:, 'min_avg_vel_from_prev'] - \
            test_trace.loc[1:, 'dist_from_prev']/
            (1+test_trace.loc[1:, 'sec_since_prev'])*3600).abs()<1.e-9).all()

        

def test_compute_time_dist_avg_vel_all_can_take_alternate_cols():
    '''
    The compute_time_dist_avg_vel_all function can accept alternate
    column names
    '''
    #First create a test trace made up of 10 random points in a 
    #1-degree square
    npts = 10
    dates = pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats = np.random.rand(10)+np.random.rand(1)[0]*89*2-89
    lons = np.random.rand(10)+np.random.rand(1)[0]*179*2-179
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'timestamp':dates,'latitude':lats,
                               'longitude':lons})
    

    test_trace = compute_time_dist_avg_vel_all(test_trace, 
                                           timestampfield='timestamp',
                                           latfield='latitude',
                                           lonfield='longitude')

    for i in range(1,npts):
        assert abs(test_trace.loc[i,'sec_since_prev'] - \
            (test_trace.loc[i, 'timestamp'] - 
             test_trace.loc[i-1,'timestamp'])/np.timedelta64(1,'s'))<1.e-9

        assert abs(test_trace.loc[i,'dist_from_prev'] - \
            great_circ_km(test_trace.loc[i-1, 'latitude'], 
                          test_trace.loc[i-1, 'longitude'],
                          test_trace.loc[i,'latitude'],
                          test_trace.loc[i,'longitude']))<1.e-9

                        
def test_min_avg_vel_all_uses_sampling_precision():
    '''
    The compute_time_dist_avg_vel_all function should use the provided
    timestamp sampling precision to compute the minimum avg velocity.
    '''
    #First create a test trace made up of 10 random points in a 
    #1-degree square
    npts = 10
    dates = pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats = np.random.rand(10)+np.random.rand(1)[0]*89*2-89
    lons = np.random.rand(10)+np.random.rand(1)[0]*179*2-179
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    

    test_trace = compute_time_dist_avg_vel_all(test_trace, 
                                           timestamp_precision=0.2)   

    
    assert (test_trace.loc[1:, 'min_avg_vel_from_prev'] == \
            test_trace.loc[1:, 'dist_from_prev']/
            (0.2+test_trace.loc[1:, 'sec_since_prev'])*3600).all()    

    
def test_time_dist_avg_all_for_probe_switches():
    '''
    The compute_time_dist_avg_vel_all function should make the first point of 
    each new trace equal to zero and functions normally on each trace.
    '''
    #First create a test trace made up of 10 random points in a 
    #1-degree square
    npts = 50
    dates = pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats = np.random.rand(50)*89*2-89
    lons = np.random.rand(50)*179*2-179
    x = np.array([[1,2,3,4,5]])
    trace_id = np.repeat(x,10)
    test_data = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    

    test_data = compute_time_dist_avg_vel_all(test_data)   
    new_probe = [0,10,20,30,40]

    
    assert all(test_data.loc[new_probe,'sec_since_prev']==-1)
    assert all(test_data.loc[new_probe,'avg_vel_from_prev']==0)
    assert all(test_data.loc[new_probe,'min_avg_vel_from_prev']==0)
    assert all(test_data.loc[new_probe,'dist_from_prev']==0)
    assert all(test_data.loc[new_probe,'heading_from_prev']==0)
    assert (test_data.loc[1:9, 'min_avg_vel_from_prev'] == \
            test_data.loc[1:9, 'dist_from_prev']/
            (1+test_data.loc[1:9, 'sec_since_prev'])*3600).all()    
    assert (test_data.loc[11:19, 'min_avg_vel_from_prev'] == \
            test_data.loc[11:19, 'dist_from_prev']/
            (1+test_data.loc[11:19, 'sec_since_prev'])*3600).all()    
    
    
    
    
def test_compute_simple_head_ch_no_heading_ch_straight_line():
    '''
    There should be no heading change for points in a straight line
    '''
    
    #Create a trace of four points in a straight line
    npts =4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[5,45,65,85]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,4)
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    
    #compute heading change
    test_trace_hc = compute_simple_head_ch(test_trace)
    
    assert test_trace_hc.loc[1,'simple_head_ch_from_prev']==0

    
def test_compute_simple_head_ch_180_heading_ch_double_back():
    '''
    There should be 180 absolute heading change for points that double back
    This checks to make sure the heading change between the middle and last point is 180
    '''
    
    #Create a trace of three points that double back
    npts =3
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    lats =[A,np.random.rand(1)[0]*90*2-90,A]
    lons = np.repeat(np.random.rand(1)[0]*180*2-180,3)
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    
    #compute heading change
    test_trace_hc = compute_simple_head_ch(test_trace)
    
    assert abs(test_trace_hc.loc[1,'simple_head_ch_from_prev'])==180    
    

    
def test_compute_simple_head_ch_no_timestampsort():
    '''
    The function provides correct results when time stamps are unsorted
    '''
    
    #Create a trace of three points that double back with out of order timestamps
    npts =3
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    lats =[A,np.random.rand(1)[0]*90*2-90,A]
    lons = np.repeat(np.random.rand(1)[0]*180*2-180,3)
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    #scramble order
    test_trace = test_trace[::-1]
    
    #compute heading change
    test_trace_hc = compute_simple_head_ch(test_trace)
    
    assert abs(test_trace_hc.loc[1,'simple_head_ch_from_prev'])==180 
    

def test_compute_simple_head_ch_90_near_equator():
    '''
    There should be 90 absolute heading change for points near equator that take a right angle
    '''
    
    #Create a trace of three points that double back
    npts =3
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[0,0,0.1]
    lons = [0,.1,.1]
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    
    #compute heading change
    test_trace_hc = compute_simple_head_ch(test_trace)
    
    assert abs(test_trace_hc.loc[1,'simple_head_ch_from_prev'])==90  
    
def test_compute_simple_head_east_positive_west_negative():
    '''
    There should be positive change turning eastward, negative change turning westward
    '''
    
    #Create a trace of three points that double back
    npts =3
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats1 =[0,0.5,0.5]
    lons1 = [0,0,.1]
    test_trace1 = pd.DataFrame({'sample_date':dates,'lat':lats1,'lon':lons1})
    lats2 =[0,0.5,0.5]
    lons2 = [0,0,-.1]
    test_trace2 = pd.DataFrame({'sample_date':dates,'lat':lats2,'lon':lons2})
    
    #Use compute _time etc to calc inputs
    test_trace1 = compute_time_dist_avg_vel(test_trace1)
    test_trace2 = compute_time_dist_avg_vel(test_trace2)
    #compute heading change
    test_trace_hc1 = compute_simple_head_ch(test_trace1)
    test_trace_hc2 = compute_simple_head_ch(test_trace2)
    
    assert (test_trace_hc1.loc[1,'simple_head_ch_from_prev']-180)<0
    assert (test_trace_hc2.loc[1,'simple_head_ch_from_prev']-180)>0
    
def test_compute_abs_head_ch_no_change_for_lines():
    '''
    Points in a straight line give no 2 point heading change
    '''
    
    npts = 4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[5,45,65,85]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,4)
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch(test_trace)
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']==0
        
    
def test_compute_abs_head_ch_360_change_for_double_back():
    '''
    Points that double back give a 360 degree heading change
    '''
    
    npts = 4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    B = np.random.rand(1)[0]*90*2-90
    lats =[A,B,A,B]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,4)
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch(test_trace)
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']==360    
    
def test_compute_abs_head_ch_360_fig_four():
    '''
    Near the equator, a figure four trace gives a 2 point change of 270
    '''
    
    npts = 4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[0,.01,.005,.005]
    lons =[1,1,.9999,1.0001]
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch(test_trace)
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']>270  
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']<270.00001
    
def test_compute_abs_head_ch_unsorted():
    '''
    The function still works with unsorted data
    '''
    npts = 4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    B = np.random.rand(1)[0]*90*2-90
    lats =[A,B,A,B]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,4)
    test_trace = pd.DataFrame({'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    test_trace = test_trace[::-1]
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch(test_trace)
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']==360 

    
    
    
    
    
def test_compute_simple_head_ch_all_no_heading_ch_straight_line():
    '''
    There should be no heading change for points in a straight line
    '''
    
    #Create a trace of four points in a straight line
    npts =4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[5,45,65,85]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,4)
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    
    #compute heading change
    test_trace_hc = compute_simple_head_ch_all(test_trace)
    
    assert test_trace_hc.loc[1,'simple_head_ch_from_prev']==0
    
    
#Heading change from previous is 180 if lat1 is positive and lat2 is negative even
#assumes moving at zero
#can you have a heading change for point 2? I'm not sure it makes sense
#
def test_compute_simple_head_ch_all_180_heading_ch_double_back():
    '''
    There should be 180 absolute heading change for points that double back
    This checks to make sure the heading change between the middle and last point is 180
    '''
    
    #Create a trace of three points that double back
    npts =3
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    lats =[A,np.random.rand(1)[0]*90*2-90,A]
    lons = np.repeat(np.random.rand(1)[0]*180*2-180,3)
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    
    #compute heading change
    test_trace_hc = compute_simple_head_ch_all(test_trace)
    
    assert abs(test_trace_hc.loc[1,'simple_head_ch_from_prev'])==180    
    

    
def test_compute_simple_head_ch_all_unsorted():
    '''
    The function provides correct results when time stamps are unsorted
    '''
    
    #Create a trace of three points that double back with out of order timestamps
    npts =3
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    lats =[A,np.random.rand(1)[0]*90*2-90,A]
    lons = np.repeat(np.random.rand(1)[0]*180*2-180,3)
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    #scramble order
    test_trace = test_trace[::-1]
    
    #compute heading change
    test_trace_hc = compute_simple_head_ch_all(test_trace)
    
    assert abs(test_trace_hc.loc[1,'simple_head_ch_from_prev'])==180 
    

def test_compute_simple_head_ch_all_90_near_equator():
    '''
    There should be 90 absolute heading change for points near equator that take a right angle
    '''
    
    #Create a trace of three points that double back
    npts =3
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[0,0,0.1]
    lons = [0,.1,.1]
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    
    #compute heading change
    test_trace_hc = compute_simple_head_ch_all(test_trace)
    
    assert abs(test_trace_hc.loc[1,'simple_head_ch_from_prev'])==90  
    
def test_compute_simple_head_all_east_positive_west_negative():
    '''
    There should be positive change turning eastward, negative change turning westward
    '''
    
    #Create a trace of three points that double back
    npts =6
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[0,0.5,0.5,0,0.5,0.5]
    lons = [0,0,.1,0,0,-.1]
    A = np.repeat(1,3)
    B= np.repeat(2,3)
    trace_id = np.append(A,B)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    #compute heading change
    test_trace_hc = compute_simple_head_ch_all(test_trace)
    
    assert (test_trace_hc.loc[1,'simple_head_ch_from_prev'] -180) <0
    assert (test_trace_hc.loc[4,'simple_head_ch_from_prev']-180)>0
    
def test_compute_abs_head_ch_no_change_for_lines():
    '''
    Points in a straight line give no 2 point heading change
    '''
    
    npts = 4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[5,45,65,85]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,npts)
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel(test_trace)
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch(test_trace)
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']==0
        
    
def test_compute_abs_head_ch_all_360_change_for_double_back():
    '''
    Points that double back give a 360 degree heading change
    '''
    
    npts = 4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    B = np.random.rand(1)[0]*90*2-90
    lats =[A,B,A,B]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,npts)
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch_all(test_trace)
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']==360    
    
def test_compute_abs_head_ch_all_360_fig_four():
    '''
    Near the equator, a figure four trace gives a 2 point change of 270
    '''
    
    npts = 4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats =[0,.01,.005,.005]
    lons =[1,1,.9999,1.0001]
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch_all(test_trace)
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']>270  
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']<270.00001
    
def test_compute_abs_head_ch_all_unsorted():
    '''
    The function still works with unsorted data
    '''
    npts = 4
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    B = np.random.rand(1)[0]*90*2-90
    lats =[A,B,A,B]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,npts)
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    test_trace = test_trace[::-1]
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch_all(test_trace)
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']==360 
    
def test_compute_abs_head_ch_all_unsorted_traces():
    '''
    The function still works with unsorted data. It returns appropriate zero values as well.
    '''
    npts = 8
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    B = np.random.rand(1)[0]*90*2-90
    lats =[A,A,B,B,A,A,B,B]
    lons =np.repeat(np.random.rand(1)[0]*180*2-180,npts)
    trace_id = [A,B,A,B,A,B,A,B]
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    test_trace = test_trace[::-1]
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch_all(test_trace)
    test_trace_hc = test_trace_hc.reset_index()
    
    assert test_trace_hc.loc[2,'abs_heading_change_last_2']==360
    assert test_trace_hc.loc[6,'abs_heading_change_last_2']==360
    assert test_trace_hc.loc[3,'abs_heading_change_last_2']==0
    assert test_trace_hc.loc[4,'abs_heading_change_last_2']==0

    
def test_compute_abs_head_ch_all_first_are_zero():
    '''
    Make sure that with multiple probes the first two points 
    will be zero
    '''
    
    npts = 8
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.rand(1)[0]*90*2-90
    B = np.random.rand(1)[0]*90*2-90
    lats =np.random.rand(8)*90*2-90
    lons =np.random.rand(8)*180*2-180
    trace_id = [A,A,A,A,B,B,B,B]
    test_trace = pd.DataFrame({'trace_id':trace_id,'sample_date':dates,'lat':lats,'lon':lons})
    
    #Use compute _time etc to calc inputs
    test_trace = compute_time_dist_avg_vel_all(test_trace)
    test_trace = test_trace[::-1]
    
    #compute heading change
    test_trace_hc = compute_abs_head_ch_all(test_trace)
    test_trace_hc = test_trace_hc.reset_index()
    
    assert test_trace_hc.loc[0,'abs_heading_change_last_2']==0
    assert test_trace_hc.loc[1,'abs_heading_change_last_2']==0
    assert test_trace_hc.loc[4,'abs_heading_change_last_2']==0
    assert test_trace_hc.loc[5,'abs_heading_change_last_2']==0
    
