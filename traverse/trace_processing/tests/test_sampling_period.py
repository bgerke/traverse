import pandas as pd
import numpy as np
from ..sampling_period import (
    get_mode,
    get_sampling_period,
    reflag_large_lag,
    detect_large_lag,
    split_on_large_lag)


def test_mode_finds_samp_rates():
    '''
    Make sure the detection of sampling rates finds the correct sampling rate
    '''
    A = np.repeat(30, 20)
    B = np.repeat(15, 60)
    C = np.random.random(10) * 90
    D = np.repeat(19, 60)
    rates = np.concatenate((A, B, C, D))
    test_trace = pd.DataFrame({'sec_since_prev': rates})
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    assert(all(test_tracer['sample_period'] == 15))


def test_smallest_mode_gets_used():
    '''
    if there are multiple modes, make sure the smallest gets used.
    This is when they are within the time stamp precision.
    '''

    A = np.repeat(16, 60)
    B = np.repeat(15, 60)
    C = np.repeat(19, 60)
    rates = np.concatenate((A, B, C))
    test_trace = pd.DataFrame({'sec_since_prev': rates})
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    assert(all(test_tracer['sample_period'] == 15))


def test_timestamp_precision_used():
    '''
    Make sure the timestamp precision is properly used
    '''

    A = np.repeat(17, 60)
    B = np.repeat(15, 60)
    C = np.repeat(23, 60)
    rates = np.concatenate((A, B, C))
    test_trace = pd.DataFrame({'sec_since_prev': rates})
    test_tracer = get_sampling_period(test_trace, timestamp_precision=2)
    # confirm all appropriate points have been flagged appropriately
    assert(all(test_tracer['sample_period'] == 15))


def test_non_integer_sampling():
    '''
    Check to see if non integer sampling rates are accepted.
    '''

    A = np.repeat(16, 60)
    B = np.repeat(15.3, 60)
    C = np.repeat(23, 60)
    rates = np.concatenate((A, B, C))
    test_trace = pd.DataFrame({'sec_since_prev': rates})
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    assert(all(test_tracer['sample_period'] == 15.3))


def test_smallest_mode_not_majority():
    '''
    When there are multiple modes, and the smallest mode is not the majority the next is found
    '''

    A = np.repeat(17, 60)
    B = np.repeat(15, 60)
    C = np.repeat(34, 60)
    rates = np.concatenate((A, B, C))
    test_trace = pd.DataFrame({'sec_since_prev': rates})
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    assert(all(test_tracer['sample_period'] == 17))


def test_mode_multiple():
    '''
    if there are multiple modes, makes sure no sampling rate is assigned
    '''

    A = np.repeat(17, 60)
    B = np.repeat(15, 60)
    C = np.repeat(19, 60)
    rates = np.concatenate((A, B, C))
    test_trace = pd.DataFrame({'sec_since_prev': rates})
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    for i in test_tracer.index:
        assert(np.isnan(test_tracer.loc[i, 'sample_period']))


def test_split_lag_trace():
    '''
    check if the trace is split correctly on a lag
    '''
    npts = 151
    A = np.repeat(30, 20)
    B = np.repeat(15, 60)
    B1 = np.random.random(1) * 9010000
    C = np.random.random(10) * 90
    D = np.repeat(19, 60)
    probe = np.repeat('probe', npts)
    rates = np.concatenate((A, B, B1, C, D))
    test_trace = pd.DataFrame(
        {'sec_since_prev': rates, 'trace_id': probe, 'trace_id': probe})
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    test_trace_split = split_on_large_lag(test_tracer)
    assert(~np.isin(test_trace_split.loc[:79, 'trace_id'].unique(
    ), (test_trace_split.loc[80:, 'trace_id'].unique())))
    assert(test_trace_split.loc[80, 'sec_since_prev'] == -1)
    assert(test_trace_split.trace_id.nunique() == 2)


def test_split_lag_trace_irregular():
    '''
    check if the trace is split correctly on a lag when it has an irregular
    sampling rate.
    '''
    npts = 13
    A = [5,5,13,13,13,13]
    B = [28,28,17,17,17,17]
    B1 = np.random.random(1) * 9010000
    probe = np.repeat('probe', npts)
    rates = np.concatenate((A, B1, B))

    test_trace = pd.DataFrame(
        {'sec_since_prev': rates, 'trace_id': probe, 'trace_id': probe})
    test_trace['sample_period']=np.nan
    test_tracer = get_sampling_period(test_trace)
    assert test_trace['sample_period'].isna().all()

    # confirm all appropriate points have been flagged appropriately
    test_trace_split = split_on_large_lag(test_tracer,lag_factor=10, min_lag=600, split_ir_samp_rate=True)
    assert(~np.isin(test_trace_split.loc[:5, 'trace_id'].unique(
    ), (test_trace_split.loc[6:, 'trace_id'].unique())))
    assert(test_trace_split.loc[6, 'sec_since_prev'] == -1)
    assert(test_trace_split.trace_id.nunique() == 2)


def test_split_lag_all_good():
    '''
    check that everything works if there is nothing to split
    '''
    npts = 20
    probe = np.repeat('probe', npts)
    rates = np.ones(npts)*5

    test_trace = pd.DataFrame(
        {'sec_since_prev': rates, 'trace_id': probe, 'trace_id': probe})
    test_trace['sample_period']=np.nan
    test_tracer = get_sampling_period(test_trace)
    
    test_trace_split = split_on_large_lag(test_tracer, split_ir_samp_rate=True)
    # confirm nothing has been split.
    assert(test_trace_split.trace_id.nunique() == 1)

def test_split_lag_trace_noncontinuous_index():
    '''
    check if the trace is split correctly on a lag even if the index
    is not continuous
    '''
    npts = 151
    A = np.repeat(30, 20)
    B = np.repeat(15, 60)
    B1 = np.random.random(1) * 9010000
    C = np.random.random(10) * 90
    D = np.repeat(19, 60)
    probe = np.repeat('probe', npts)
    rates = np.concatenate((A, B, B1, C, D))

    index = np.arange(npts)*2
    np.random.shuffle(index)
    test_trace = pd.DataFrame(
        {'sec_since_prev': rates, 'trace_id': probe, 'trace_id': probe},
        index=index)
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    test_trace_split = split_on_large_lag(test_tracer)
    assert(~np.isin(
        test_trace_split.loc[test_trace_split.index[:79], 'trace_id'].unique(
    ), (test_trace_split.loc[test_trace_split.index[80:], 'trace_id'].unique())))
    assert(test_trace_split.loc[test_trace_split.index[80], 'sec_since_prev'] == -1)
    assert(test_trace_split.trace_id.nunique() == 2)

def test_split_multiple_lag_trace():
    '''
    if there are multiple lags, check to see if the trace splits properly.
    Check to make sure exactly three splits occur.
    '''
    npts = 151
    A = np.repeat(30, 19)
    A1 = np.random.random(1) * 9010000
    B = np.repeat(15, 60)
    B1 = np.random.random(1) * 9010000
    C = np.random.random(10) * 90
    D = np.repeat(19, 60)
    probe = np.repeat('probe', npts)
    rates = np.concatenate((A, A1, B, B1, C, D))
    test_trace = pd.DataFrame(
        {'sec_since_prev': rates, 'trace_id': probe, 'trace_id': probe})
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    test_trace_split = split_on_large_lag(test_tracer)
    assert(~np.isin(test_trace_split.loc[:18, 'trace_id'].unique(
    ), (test_trace_split.loc[19:79, 'trace_id'].unique())))
    assert(~np.isin(test_trace_split.loc[:18, 'trace_id'].unique(
    ), (test_trace_split.loc[80:, 'trace_id'].unique())))
    assert(~np.isin(test_trace_split.loc[80:, 'trace_id'].unique(
    ), (test_trace_split.loc[19:79, 'trace_id'].unique())))
    assert(test_trace_split.loc[19, 'sec_since_prev'] == -1)
    assert(test_trace_split.loc[80, 'sec_since_prev'] == -1)
    assert(test_trace_split.trace_id.nunique() == 3)


def test_detect_large_lag():
    '''
    check if a large lag is properly detected
    '''
    npts = 101
    A = np.repeat(30, 50)
    B = np.random.random(1) * 9010000
    C = np.repeat(30, 50)
    probe = np.repeat('probe', npts)
    rates = np.concatenate((A, B, C))
    test_trace = pd.DataFrame(
        {'sec_since_prev': rates, 'trace_id': probe, 'trace_id': probe})
    test_tracer = get_sampling_period(test_trace)
    # confirm all appropriate points have been flagged appropriately
    test_trace_lag = detect_large_lag(test_tracer, lag_factor=10,min_lag=600,split_ir_samp_rate=True)
    assert(test_trace_lag.loc[50, 'long_lag_flag'] == 1)
    assert(all(test_trace_lag.loc[:49, 'long_lag_flag'] == 0))
    assert(all(test_trace_lag.loc[51:, 'long_lag_flag'] == 0))

def test_split_lag_trace_sampling_period_recalculated():
    '''
    check if the trace is split correctly on a lag.
    Make sure the sample rate has been recalculated
    '''
    npts = 151
    A = np.repeat(30, 20)
    B = np.repeat(15, 60)
    B1 = np.random.random(1) * 9010000
    C = np.random.random(10) * 90
    D = np.repeat(19, 60)
    probe = np.repeat('probe', npts)
    rates = np.concatenate((A, B, B1, C, D))
    test_trace = pd.DataFrame(
        {'sec_since_prev': rates, 'trace_id': probe, 'trace_id': probe})
    test_tracer = get_sampling_period(test_trace)
    assert(test_tracer['sample_period'].nunique()==1)
    # confirm all appropriate points have been flagged appropriately
    test_trace_split = split_on_large_lag(test_tracer)
    assert(test_trace_split.loc[:79,'sample_period'].unique()==15)
    assert(test_trace_split.loc[80:,'sample_period'].unique()==19)
