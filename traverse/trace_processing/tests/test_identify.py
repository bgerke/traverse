#Test the identification of unique traces
import pandas as pd
import pytest
from ..identify import create_unique_trace_ids

def test_create_unique_trace_ids_probe_only():
    '''
    create_unique_trace_ids generates a unique trace per probe ID
    if there is no provider ID
    '''

    data = pd.DataFrame(
        {'probe_id':[1,1,1,2,2,3,4,4,4,4]}
    )

    data = create_unique_trace_ids(data, providerfield=None)

    assert len(data['trace_id'].unique())==4
    assert len(data.groupby('probe_id')['trace_id'].unique())==4

def test_create_unique_trace_ids_probe_and_provider():
    '''
    create_unique_trace_ids generates a unique trace per unique probe/provider
    combination
    '''

    data = pd.DataFrame(
        {'probe_id':[1,1,1,2,2,3,4,4,4,4],
         'probe_data_provider':[1,1,1,1,1,1,1,1,2,2]}
    )

    data = create_unique_trace_ids(data)

    assert len(data['trace_id'].unique())==5
    assert len(
        data.groupby(
            ['probe_id','probe_data_provider']
        )['trace_id'].first().unique())==5

def test_create_unique_trace_ids_alt_fields():
    '''
    create_unique_trace_ids generates a unique trace per unique probe/provider
    combination
    '''

    data = pd.DataFrame(
        {'probe':[1,1,1,2,2,3,4,4,4,4],
         'provider':[1,1,1,1,1,1,1,1,2,2]}
    )

    data = create_unique_trace_ids(data, 
                                   probefield='probe', 
                                   providerfield='provider')

    assert len(data['trace_id'].unique())==5
    assert len(
        data.groupby(
            ['probe','provider']
        )['trace_id'].first().unique())==5

def test_create_unique_ids_with_missing_data():
    '''
    create_unique_trace_ids can handle missing (NaN) data in the
    probefield or providerfield columns
    '''
    data = pd.DataFrame(
        {'probe_id':[1,1,1,2,2,3,4,4,4,4],
         'probe_data_provider':[1,1,1,1,1,1,1,1,2,2]}
    )

#add some rows with missing data
    data = data.append(
        pd.DataFrame({'probe_id':[4,5,5]}), sort=False).append(
            pd.DataFrame({'probe_data_provider':[1]}), 
            sort=False).reset_index(drop=True)

    data = create_unique_trace_ids(data)

    assert len(data['trace_id'].unique())==6 #one of these should be NaN

    #If we do a groupby, it should ignore the missing data.
    assert len(
        data.groupby(
            ['probe_id','probe_data_provider']
        )['trace_id'].first().unique())==5

    assert (~data.loc[:9, 'trace_id'].isna()).all()
    assert (data.loc[10:, 'trace_id'].isna()).all()
