import pandas as pd
import numpy as np
from ..crowflies import (compute_time_dist_avg_vel, compute_abs_head_ch, 
                         compute_simple_head_ch, compute_time_dist_avg_vel_all, 
                         compute_simple_head_ch_all, compute_abs_head_ch_all)
from ...utils.coords import great_circ_km, great_circ_head
from ..stationary import (find_stationary_points, stationary_loop,
                         calc_dur,continuity_chop)






def test_find_stationary_points_all_in_a_row():
    '''
    flag all the appropriate stay points from a random generation of fifty in a row
    '''
    npts = 70
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A =  np.random.random(10)*90 
    B = np.repeat(np.random.rand(1)[0]*90,50) + np.random.rand(50)*.0001
    C = np.random.random(10)*90
    lats = np.concatenate((A,B,C))
    A =  np.random.random(10)*180 
    B = np.repeat(np.random.rand(1)[0]*180,50) + np.random.rand(50)*.0001
    C = np.random.random(10)*180
    lons = np.concatenate((A,B,C))
    #make a trace with 10 stationary points in the middle
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id, 'sample_date':dates,'lat':lats,'lon':lons})
    test_trace = compute_time_dist_avg_vel(test_trace)
    test_trace_hc = compute_abs_head_ch(test_trace)
    #find the stay points
    test_trace_stay = find_stationary_points(test_trace_hc, is_sorted=True)
    #confirm all appropriate points have been flagged
    assert all(test_trace_stay.loc[10:59, 'stationary_point_flag']==1)

    
def test_stationary_points_flag_work_multiple_stay_points():
    '''
    With two large stay points the program properly identifies different stay points and 
    flags them correctly
    '''
    npts = 150
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.random(10)*90 
    B = np.repeat(np.random.rand(1)[0]*90,60) + np.random.rand(60)*.00001
    C = np.random.random(10)*90
    D = np.repeat(np.random.rand(1)[0]*90,60) + np.random.rand(60)*.00001
    E = np.random.random(10)*90
    lats = np.concatenate((A,B,C,D,E))
    A = np.random.random(10)*180 
    B = np.repeat(np.random.rand(1)[0]*180,60) + np.random.rand(60)*.00001
    C = np.random.random(10)*180
    D = np.repeat(np.random.rand(1)[0]*180,60) + np.random.rand(60)*.00001
    E = np.random.random(10)*180
    lons = np.concatenate((A,B,C,D,E))
    #make a trace with 10 stationary points in the middle
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id, 'sample_date':dates,'lat':lats,'lon':lons})
    test_trace = compute_time_dist_avg_vel(test_trace)
    test_trace_hc = compute_abs_head_ch(test_trace)
    #find the stay points
    test_trace_stay = find_stationary_points(test_trace_hc, is_sorted=True)
    #confirm all appropriate points have been flagged
    assert all(test_trace_stay.loc[10:69, 'stationary_point_flag']==1)
    assert all(test_trace_stay.loc[80:139, 'stationary_point_flag']==2)
    assert all(test_trace_stay.loc[0:9, 'stationary_point_flag']==-1)
    
def test_find_stationary_points_two_stay_seperated_by_time_not_distance():
    '''
    Two stay points in the same place but seperated by a legitimate trace will 
    receive two unique flags'''
    npts = 150
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.random.random(10)*90 
    B = np.repeat(np.random.rand(1)[0]*90,60) + np.random.rand(60)*.00001
    C = np.random.random(10)*90
    D = np.random.random(10)*90
    lats = np.concatenate((A,B,C,B,D))
    A = np.random.random(10)*180 
    B = np.repeat(np.random.rand(1)[0]*180,60) + np.random.rand(60)*.00001
    C = np.random.random(10)*180
    D = np.random.random(10)*180
    lons = np.concatenate((A,B,C,B,D))
    #make a trace with 10 stationary points in the middle
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id, 'sample_date':dates,'lat':lats,'lon':lons})
    test_trace = compute_time_dist_avg_vel(test_trace)
    test_trace_hc = compute_abs_head_ch(test_trace)
    #find the stay points
    test_trace_stay = find_stationary_points(test_trace_hc, is_sorted=True)
    #confirm all appropriate points have been flagged
    assert all(test_trace_stay.loc[10:69, 'stationary_point_flag']==1)
    assert all(test_trace_stay.loc[80:139, 'stationary_point_flag']==2)
    assert all(test_trace_stay.loc[0:9, 'stationary_point_flag']==-1)
    
def test_find_stationary_points_early_no_stay():
    '''
    Points early on in a trace are within the radius of a stay point later on. They should not be
    flagged but the stay point should be.'''
    npts = 80
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.repeat(0,10) 
    B = np.repeat(np.random.rand(1)[0]*.001,60) + np.random.rand(60)*.00001
    C = np.random.random(10)*90
    lats = np.concatenate((A,C,B))
    A = np.repeat(0,10) 
    B = np.repeat(np.random.rand(1)[0]*.001,60) + np.random.rand(60)*.00001
    C = np.random.random(10)*180
    lons = np.concatenate((A,C,B))
    #make a trace with 10 stationary points in the middle
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id, 'sample_date':dates,'lat':lats,'lon':lons})
    test_trace = compute_time_dist_avg_vel(test_trace)
    test_trace_hc = compute_abs_head_ch(test_trace)
    #find the stay points
    test_trace_stay = find_stationary_points(test_trace_hc, is_sorted=True)
    #confirm all appropriate points have been flagged
    assert all(test_trace_stay.loc[20:79, 'stationary_point_flag']==1)
    assert all(test_trace_stay.loc[0:19, 'stationary_point_flag']==-1)
    
def test_find_stationary_points_early_no_stay_two_total():
    '''
    Points early on in a trace are within the radius of a stay point later on. They should not be
    flagged but the stay point should be. both later stay points should be flagged.'''
    npts = 150
    dates =pd.date_range('01-01-2018', periods=npts, freq='min').values
    A = np.repeat(0,10) 
    B = np.repeat(np.random.rand(1)[0]*.001,60) + np.random.rand(60)*.00001
    C = np.random.random(10)*90
    lats = np.concatenate((A,C,B,C,B))
    A = np.repeat(0,10) 
    B = np.repeat(np.random.rand(1)[0]*.001,60) + np.random.rand(60)*.00001
    C = np.random.random(10)*180
    lons = np.concatenate((A,C,B,C,B))
    #make a trace with 10 stationary points in the middle
    trace_id = np.repeat(1,npts)
    test_trace = pd.DataFrame({'trace_id':trace_id, 'sample_date':dates,'lat':lats,'lon':lons})
    test_trace = compute_time_dist_avg_vel(test_trace)
    test_trace_hc = compute_abs_head_ch(test_trace)
    #find the stay points
    test_trace_stay = find_stationary_points(test_trace_hc, is_sorted=True)
    #confirm all appropriate points have been flagged
    assert all(test_trace_stay.loc[20:79, 'stationary_point_flag']==1)
    assert all(test_trace_stay.loc[0:19, 'stationary_point_flag']==-1)
    assert all(test_trace_stay.loc[80:89, 'stationary_point_flag']==-1)
    assert all(test_trace_stay.loc[90:, 'stationary_point_flag']==2)
    
    
    
