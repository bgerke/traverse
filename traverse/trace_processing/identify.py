#Functions for identifying unique traces
from uuid import uuid4
from traverse.settings import *

def create_unique_trace_ids(data, 
                            probefield=PROBEFIELD, 
                            providerfield=PROVIDERFIELD,
                            tracefield='trace_id'):
    '''
    Assign UUIDs to unique traces based on the probe ID and the provider ID.
    If all data comes from the same provider, set provider_field to None,
    and the trace_id will just be based on the unique probe IDs.
    
    Arguments:
    ----------
    data: A pandas DataFrame of GPS data with, at minimum, a field identifying
        unique probes from a given provider and possibly also a field
        identifying the provider.

    Keywords:
    ---------
    probefield: The column name containing the unique probe ID within a 
        provider.
    providerfield: The column name containing the identity of the data 
        provider. If set to None, it is assumed that all probe IDs are 
        fully unique.
    tracefield: The name of the field to use for identifying unique traces.
        This field will be created if it does not exist already.

    '''

    #Group on probefield
    group_cols = [probefield]    
    #Consider only rows that have data in the required fields.
    good = ~data[probefield].isna()

    #If there is a provider field, also use that for grouping
    if providerfield:
        group_cols += [providerfield]
        good &= ~data[providerfield].isna()

    data.loc[good,tracefield] = ''

    data.loc[good,tracefield] = \
        data.loc[good].groupby(
            group_cols, sort=False).trace_id.transform(lambda g: uuid4())
            
    return data