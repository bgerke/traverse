# Miscellaneous tools and helper functions
import pandas as pd
import numpy as np
from traverse.settings import *

def get_date_columns(filename, match_strings=['time', 'date']):
    '''
    Inspect the column headers in a csv file and return the ones that
    appear to be time or date related.

    Arguments:
    ----------
    filename: path to a CSV file.

    Keywords:
    ---------
    match_strings: A list of strings whose presence in a column indicates
        a date field.

    ''' 

    assert filename[-3:] == 'csv', \
        'Input to get_date_columns must be a csv file.'

    if type(match_strings) == type(''):
        # wrap a string-only input in a list,so we don't
        # iterate over the characters.
        match_strings = [match_strings]

    cols = pd.read_csv(filename, nrows=1, header=None)
    output = []
    for c in cols.iloc[0].values:
        for ms in match_strings:
            if ms.lower() in c.lower():
                output += [c]
                break
    return output


def error_group(data, probe_data_provider=PROVIDERFIELD):
    '''
    Group the error flags to analyze what errors you have gotten. 
    This yields a data frame with every combination of errors
    and the counts for how many times that combination occurs.

    INPUTS
    ---------

    data - the data that has been run through the flagging pipeline.
    '''

    n = len(data)
    dg = data.groupby('error_flag').agg({list(data)[0]: 'count', 'trace_id': 'nunique',
                                         probe_data_provider: 'nunique'}).rename(index=str, columns={list(data)[0]: '# Points',
                                                                                                       'trace_id': '# Traces',
                                                                                                       probe_data_provider: '# Providers'})
    dg['% Points'] = dg['# Points']/n*100
    dg['% Traces'] = dg['# Traces'] / data.trace_id.nunique()*100
    dg.reset_index(inplace=True)
    dg['Error types'] = pd.to_numeric(dg.error_flag).apply(lambda x:
                                                           [len(bin(x))-i-1 for i in range(2, len(bin(x))) if int(bin(x)[i])])
    dg.set_index('error_flag', inplace=True)
    return dg


def error_list(data, num_flags=10):
    '''
    Make a list of how many times each type of error occurs. 
    This does not account for overlapping errors.

    INPUTS
    ---------

    data - the data that has been run through the flagging pipeline.

    Keywords
    --------

    num_flags - the number of possible flags in the error pipeline.


    '''

    z = np.zeros(num_flags+1).astype(int)
    n = len(data)
    t = data.trace_id.nunique()
    dl = pd.DataFrame({'Error type': list(range(11)), '# of Points': z.astype(int), '# of Traces': z.astype(int),
                       '# of Providers': z.astype(int), '% of Points': z, '% of Traces': z})
    dl.loc[-1, 'Error type'] = 'No Errors'
    dl.sort_index(inplace=True)
    p = data.loc[(data.error_flag == 0)]
    t1 = data.loc[(data.error_flag == 0)].trace_id.nunique()
    dl.loc[-1, '# of Points'] = int(len(p))
    dl.loc[-1, '% of Points'] = len(p)/n*100
    dl.loc[-1, '# of Traces'] = int(t1)
    dl.loc[-1, '% of Traces'] = t1/t*100
    dl.loc[-1, '# of Providers'] = data.loc[(
        data.error_flag == 0)].probe_data_provider.nunique()
    dl.set_index('Error type', inplace=True)
    for f in range(11):
        p = data.loc[(data.error_flag & 2**f) > 0]
        t1 = data.loc[(data.error_flag & 2**f) > 0].trace_id.nunique()
        dl.loc[f, '# of Points'] = int(len(p))
        dl.loc[f, '% of Points'] = len(p)/n*100
        dl.loc[f, '# of Traces'] = int(t1)
        dl.loc[f, '% of Traces'] = t1/t*100
        dl.loc[f, '# of Providers'] = data.loc[(
            data.error_flag & 2**f) > 0].probe_data_provider.nunique()
    dl.astype({'# of Points': int, '# of Traces': int,
               '# of Providers': int}).round(2)
    return dl

def parallel_by_group(data, thread_count, grouping_variable = PROBEFIELD):
    '''
    This function prepares data for parallelization.
    It splits the data by the variable set in grouping variable.
    The function returns returns an array of data frames that represent 
    a partition of the dataset, which are roughly equal-sized, 
    while ensuring that like values of the grouping variable are all 
    contained within the same partition. 
    This can then be used, for example, to do computations in parallel 
    that operate on the groups represented by the grouping variable.
    
    Inputs
    ------
    
    Data - the data to be grouped
    
    Thread_count - the number of processors you wish to use in parallelizing
    
    Keywords
    -----
    
    grouping_variable: The variale containing the groups you wish to preserve when chunking the data
    
    '''
    
    #Split into roughly equal size chunks preserving groups
    #get the counts of each trace
    group_counts = pd.DataFrame(data[grouping_variable].value_counts())
    #make a list how we will split traces
    #this snaking from low to high and back again ensures an aproximately equal
    #distribution of small groups and big groups in each split
    #For example with three threads you'd get 012332100123321 and so on
    #With groups sorted by size you quickly get approximately equal groups
    threadkey = list(range(thread_count)) + list(reversed(range(thread_count)))
    #expand the list to the size of the data
    group_counts['chopcode']= pd.np.tile(threadkey, len(group_counts) // len(threadkey)).tolist() + threadkey[:len(group_counts)%len(threadkey)]
    ret_list = []
    for thread in range(thread_count):
        groups = group_counts.loc[group_counts['chopcode']==thread].index
        ret_list.append(data.loc[data[grouping_variable].isin(groups)]) 
    return ret_list

