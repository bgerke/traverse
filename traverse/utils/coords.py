# -*- coding: utf-8 -*-
"""
Created on Mon Apr 30 14:01:13 2018

Tools for handling lat/lon coordinates, calculating great circle
distances, etc.

@author: bgerke
"""
import numpy as np

EARTH_RADIUS = 6378137. #In meters at the equator

def validate_lat_lon(lat, lon):
    '''
    Make sure that latitute and longitude are within acceptable
    boundaries.

    lat and lon must have type numpy.ndarray
    '''
    assert ((type(lat)==np.ndarray) and (type(lon)==np.ndarray)), \
        'Inputs to validate_lat_lon must be numpy arrays.'

    assert ((-90<=lat<=90).all() and (-180<=lon<=180).all()), \
        'Invalid lat/lon coordinates encountered.'

def great_circ_km(lat1, lon1, lat2, lon2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    All args must be scalar or numpy-array-like. Both pairs of lat/lon 
    inputs must be of equal length (i.e., len(lat1)==len(lat2) must be True).
    Also, either all four inputs must be of equal length, or one pair
    must be scalar.

    The inputs are assumed to be valid latitude and longitude values, in 
    degress (i.e., between -90 and 90 for latitude and between 
    -180 and 180 for longitude). It is recommended to run
    new data through validate_lat_lon before computing distances.

    Adopted from SO thread at https://bit.ly/2HUWgeE

    Returns the great-circle distance in kilometers

    """
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = (np.sin(dlat/2.0)**2 +
         np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2.0)**2)
    sqrta = np.array(np.nan_to_num(np.sqrt(a)))
    gt1 = sqrta>1
    #limit sqrt(a) to be strictly <= 1. Right now it can be
    #slightly larger due to numerical precision.
    sqrta[gt1] = 1
    c = 2 * np.arcsin(sqrta) 
    km = EARTH_RADIUS * c / 1000.
    km = np.nan_to_num(km)
    return km

def great_circ_head(lat1, lon1, lat2, lon2):
    '''
    Compute the great circle heading between one lat/long pair and another
    Formula:
    θ = atan2( sin Δλ ⋅ cos φ2 , cos φ1 ⋅ sin φ2 − sin φ1 ⋅ cos φ2 ⋅ cos Δλ )
    where φ1,λ1 is the start point, φ2,λ2 the end point
    (Δλ is the difference in longitude)

    All args must be scalar or numpy-array-like of equal length. They
    are assumed to be valid latitude and longitude values, in degress
    (i.e., between -90 and 90 for latitude and between 
    -180 and 180 for longitude). It is recommended to run
    new data through validate_lat_lon before computing heading.

    Returns the heading in degrees along a great circle arc
    from point 1 to point 2.
    '''
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])
    dlon = lon2-lon1
    heading = np.arctan2(np.sin(dlon)*np.cos(lat2),
                         np.cos(lat1)*np.sin(lat2)-
                         np.sin(lat1)*np.cos(lat2)*np.cos(dlon))
    return (heading*180/np.pi+360) % 360

def lat2wmy(lat):
    '''Convert a latitude or numpy array of latitutes
    to web mercator coordinates'''
    return np.log(
            np.tan(np.pi/4 + np.radians(lat)/2)) * EARTH_RADIUS

def lon2wmx(lon):
    '''Convert a longitude or numpy array of longitudes
    to web mercator coordinates'''
    return np.radians(lon) * EARTH_RADIUS
