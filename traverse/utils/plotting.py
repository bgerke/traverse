import pandas as pd
import numpy as np
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, CDSView, BooleanFilter
from bokeh.tile_providers import CARTODBPOSITRON as osmtile
from traverse.utils.coords import lon2wmx, lat2wmy

def plot_trace_on_map(trace, 
                      timestampfield='sample_date', latfield='lat', 
                      lonfield='lon',
                      is_sorted=False, size=8, connected=True, 
                      **kwargs):
    '''
    Plot a trace on a map tile.
     Inputs
     -------
     trace - A dataframe (or group object) that contains a timestamp,
     latitude, and longitute field for at least two points.
     
     Keywords
     --------
     fig - a bokeh figure object.
     
     timestampfield, latfield, lonfield - Names of the timestamp, latitute
     and longitude column headers in the input trace.
     
     is_sorted: If true, then the input trace is sorted in ascending
     order of timestamp, so we can skip sorting it internally.
      
     size: point size for trace points
     
     connected: whether or not to draw a line connecting the points
     in the trace
    '''

    seconds = (trace[timestampfield] - 
               trace[timestampfield].min()).apply(
                       pd.Timedelta.total_seconds).values.astype(int)

    
    trace.loc[:,'x'] = lon2wmx(trace.loc[:,'lon'])
    trace.loc[:,'y'] = lat2wmy(trace.loc[:,'lat'])

    #The below creates a red-to-blue color map running from 
    #the beginning to the end of the seconds array.
    colors = np.array(["#%02x%02x%02x" % (r,0,b) 
                       for b,r in zip(seconds*255//np.max(seconds), 
                                      seconds[::-1]*255//np.max(seconds))])

    trace.loc[:,'color'] = colors

    TOOLTIPS = [
            ('index', '$index'),
            ('(lat,lon)', '(@lat,@lon)'),
            ('sec from prev','@sec_since_prev'),
            ('km from prev','@dist_from_prev'),
            ('km/h from prev', '@min_avg_vel_from_prev'),
            ]
    fig=figure(x_axis_type='mercator', y_axis_type='mercator', 
             toolbar_location='right', tools='hover,pan,wheel_zoom,reset',
             tooltips=TOOLTIPS, **kwargs)

    source = ColumnDataSource(trace)
    fig.line('x','y', source=source,
        color='black', name='traceline')
    
    fig.circle('x','y', source=source,
             fill_color='color', line_color='color', size=size,
             name='tracepts')
   
    fig.add_tile(osmtile)
    
    return fig

def plot_bad_trace(trace, maxvel=200, size=8, **kwargs):
    '''
    Plot a trace on a map, flagging "bad" points with calculated
    point-to-point velocity exceeding a specified threshold.
    
    Inputs
    ------
    trace - A dataframe (or group object) that contains a timestamp,
     latitude, and longitute field for at least two points.
     
     Keywords
     --------
     
    '''
    
    p = plot_trace_on_map(trace, size=size, **kwargs)
    source=[r for r in p.renderers if r.name=='tracepts'][0].data_source
    view = CDSView(
        source=source, 
        filters=[BooleanFilter([
            True if v > maxvel else False \
                    for v in source.data['min_avg_vel_from_prev']]),
                ]
        )
    
    p.circle_x('x','y', source=source, view=view,
                fill_color='yellow', line_color='black', 
               size=size*1.5, name='traceptsbad')
    return p

