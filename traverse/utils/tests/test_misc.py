
import pytest
import os
from ..misc import (get_date_columns)

def test_get_date_columns_requires_csv():
    '''
    get_date_columns throws an assertion error if it doesn't
    receive a csv file
    '''

    with pytest.raises(AssertionError):
        get_date_columns('foo.bar')

def test_get_date_columns():
    '''
    Test that get_date_columns returns the expected columns
    both in the default case and with a specified set of match strings
    '''
    dir_path = os.path.dirname(os.path.realpath(__file__))
    assert get_date_columns(
        os.path.join(dir_path,'test_data/test_columns.csv')) == \
        ['timestamp', 'birth_date', 'TIME.FRAME']
    
    assert get_date_columns(
        os.path.join(dir_path,'test_data/test_columns.csv'), 
        match_strings=['foo']) == \
        ['foo_bar']