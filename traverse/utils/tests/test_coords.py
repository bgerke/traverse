from .. import coords
import numpy as np
import pytest

def test_earth_radius():
    '''
    Test that earth's radius is as expected
    '''
    assert coords.EARTH_RADIUS == 6378137.

def test_no_movment_no_distance():
    '''
    The distance between identical coordinates should be zero.
    '''
    #Pick a random lat/lon
    lat = np.random.rand(1)*(90*2)-90.
    lon = np.random.rand(1)*360-180.

    assert coords.great_circ_km(lat,lon,lat,lon) == 0

def test_zero_heading_between_identical_points():
    '''
    The heading between identical points should be zero.
    '''
    #Pick a random lat/lon
    lat = np.random.rand(1)*(90*2)-90.
    lon = np.random.rand(1)*360-180.

    assert coords.great_circ_head(lat,lon,lat,lon) == 0

def test_can_compute_dist_hdg_with_scalars():
    '''
    Can give scalar inputs to distance and heading functions
    '''
    assert coords.great_circ_head(0,0,0,0) == 0
    assert coords.great_circ_km(0,0,0,0) == 0

def test_latitude_longitude_out_of_range():
    '''
    Lat/lon values outside of allowable ranges should
    raise an error in validate_lat_lon
    '''
    #Pick a random lat/lon pair
    lat = np.random.rand(1)*(90*2)-90.
    lon = np.random.rand(1)*360-180.
    with pytest.raises(AssertionError):
        coords.validate_lat_lon(lat+181, lon)
        coords.validate_lat_lon(lat-181,lon)
        coords.validate_lat_lon(lat ,lon+361)
        coords.validate_lat_lon(lat, lon-361)

def test_one_degree_latitude_distance():
    '''
    Moving one degree in latitude, in either direction, while
    holding longitude fixed should give
    the expected distance from a simple geometric calculation
    '''
    #Pick a random latitude between -89 and 89 degrees
    #and a random longitude anywhere
    lat = np.random.rand(1)*(89*2)-89.
    lon = np.random.rand(1)*360-180.
    expected = np.radians(1)*coords.EARTH_RADIUS/1000.
    assert (coords.great_circ_km(lat,lon,lat+1,lon) - expected) < 1.e-9
    assert (coords.great_circ_km(lat,lon,lat-1,lon) - expected) < 1.e-9

def test_one_degree_longitude_distance_equator():
    '''
    Moving one degree in longitude, in either direction, on
    the equator should give the expected distance from a
    simple geometric calculation from the angle and radius
    '''
    lat = np.array([0])
    lon = np.random.rand(1)*360-180.
    expected = np.radians(1)*coords.EARTH_RADIUS/1000.
    assert (coords.great_circ_km(lat,lon,lat,lon+1) - expected) < 1.e-9
    assert (coords.great_circ_km(lat,lon,lat,lon+1) - expected) < 1.e-9

def test_one_degree_longitude_distance_pole():
    '''
    Moving one degree in longitude, in either direction, while at either
    pole should give zero distance
    '''
    lat = np.array([90])
    lon = np.random.rand(1)*360-180.
    assert (coords.great_circ_km(lat,lon,lat,lon+1) - 0) < 1.e-9
    assert (coords.great_circ_km(lat,lon,lat,lon+1) - 0) < 1.e-9
    assert (coords.great_circ_km(-lat,lon,-lat,lon+1) - 0) < 1.e-9
    assert (coords.great_circ_km(-lat,lon,-lat,lon+1) - 0) < 1.e-9

def test_great_circle_heading_lat_only():
    '''
    Moving to a higher (lower) latitude should give
    a heading of 0 (180) if longitude is fixed
    '''

    #Pick a random latitude between -89 and 89 degrees
    #and a random longitude anywhere
    lat = np.random.rand(1)*(89*2)-89.
    lon = np.random.rand(1)*360-180.
    assert coords.great_circ_head(lat,lon,lat+1,lon) == 0
    assert coords.great_circ_head(lat,lon,lat-1,lon) == 180

def test_great_circle_heading_lon_only():
    '''
    Moving to a higher (lower) longitude should give a
    heading of 90 (270) if latitude is fixed at zero
    '''
    #Pick latitude on the equator and a random
    #longitude between -179 and 179.
    lat = np.array([0])
    lon = np.random.rand(1)*(179*2)-179.
    assert coords.great_circ_head(lat,lon,lat,lon+1) == 90
    assert coords.great_circ_head(lat,lon,lat,lon-1) == 270

def test_great_circle_heading_right_general_direction():
    '''
    Make sure that the great circle heading points in
    the right general direction at all other latitudes
    (this ensures that we're properly converting to radians)
    '''
    lat = np.arange(90)
    lon = np.random.rand(1)*(179*2)-179.
    assert (coords.great_circ_head(lat,lon,lat,lon+1) <= 180).all()
    assert (coords.great_circ_head(lat,lon,lat,lon-1) >= 180).all()
