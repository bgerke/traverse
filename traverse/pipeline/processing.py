from datetime import datetime
import pandas as pd
from traverse.trace_processing.crowflies import (
    compute_time_dist_avg_vel_all, compute_abs_head_ch_all)
from traverse.trace_processing.identify import (
    create_unique_trace_ids)
from traverse.trace_processing.sampling_period import(
    get_sampling_period, split_on_large_lag)
from traverse.error_flagging import (
    flag_missing_data,
    flag_duplicate_timestamps,
    flag_singleton_traces,
    flag_stuck_locations,
    flag_velocity_outliers,
    flag_acute_position_outliers,
    flag_sample_period_anomalies,
    flag_excessive_anomaly_fractions,
    segregate_flagged_errors, ERROR_FLAGS)
from traverse.trace_processing.stationary import find_stationary_points
import os
import sys
from traverse.settings import * 


def flag_singletons(data, err_data=None, start_time=None,
                    recombine_data=False):
    '''
    Convenience function to flag all singleton traces in a dataset, 
    print progress information, and segregate the flagged points.

    Returns a dataset with rows flagged that are singleton traces. 
    If recombine_data=False, the "clean" and flagged
    datasets are returned in separate dataframes.

    Arguments:
    ----------
    data: A dataset containing GPS data for one or more probes

    Keyword Arguments:
    ------------------
    err_data: A dataframe containing erroneous data from upstream
        calculations in the pipeline.
    start_time: A timestamp representing the time at which the current 
        pipeline process started. If not provided, the current time will
        be used.
    recombine_data: if True, return a single dataset, with errors flagged. 
        If False, return two datasets, the first one being "clean" and the
        second one containing all of the data that was flagged in this step.
    '''

    if not start_time:
        start_time = datetime.now()
    if err_data is None:
        nerr = 0
    else:
        nerr = len(err_data)
    print('{} Flagging singleton traces.'.format(
        datetime.now()-start_time))
    data = flag_singleton_traces(data, tracefield='trace_id')
    # Segregate points that were flagged
    data, err_data = segregate_flagged_errors(data, err_data)
    derr = len(err_data) - nerr
    nerr = len(err_data)

    print('{t} Flagged {n} rows; kept {m}'.format(
        t=datetime.now()-start_time, n=derr, m=len(data)))

    if not recombine_data:
        return data, err_data

    # Combine good and bad data into a single data set
    data = pd.concat([data, err_data], sort=False)
    return data


def flag_missing_and_duplicate(data, err_data=None, start_time=None,
                               probefield=PROBEFIELD,
                               tracefield='trace_id',
                               timestampfield=TIMESTAMPFIELD,
                               providerfield=PROVIDERFIELD,
                               latfield=LATFIELD, lonfield=LONFIELD,
                               do_upstream=False,
                               recombine_data=True):
    '''
    Flag points with missing data in key fields, and points
    that have non-unique timestamps.

    Returns a dataset with rows flagged that have missing data or non-unique 
    timestamps If recombine_data=False, the "clean" and flagged
    datasets are returned in separate dataframes.

    Arguments:
    ----------
    data: A dataset containing GPS data for one or more probes

    Keyword Arguments:
    ------------------
    start_time: A timestamp representing the time at which the current 
        pipeline process started. If not provided, the current time will
        be used.
    err_data: a dataframe containing data that has been previously
        flagged as erroneous and segregated from the main dataset (if any).
    probefield: The name of the field identifying individual probes within
        the dataset
    tracefield: The name of the field identifying unique traces within the 
        dataset
    timestampfield: The name of the field containing timestamp data for each 
        GPS point
    providerfield: The name of the field identifying the provider of the
        probe data. If None, all data is assumed to be from the same provider
        (i.e., all probe IDs are assumed to be unique).
    latfield, lonfield: The names of the fields containing latitude and 
        longitude data
    do_upstream: if True, also do all upstream calculations that are expected 
        to be needed to prepare an unprocessed dataset for this step in the 
        pipeline
    recombine_data: if True, return a single dataset, with errors flagged. 
        If False, return two datasets, the first one being "clean" and the
        second one containing all of the data that was flagged in this step.
    '''

    if not start_time:
        start_time = datetime.now()

    if do_upstream:  # Do upstream error-processing steps
        # Create unique trace IDs by probe and provider
        print('{} Generating unique trace IDs'.format(
            datetime.now()-start_time))
        data = create_unique_trace_ids(data,
                                       probefield=probefield,
                                       providerfield=providerfield,
                                       tracefield=tracefield)

    # Flag rows with missing data
    print('{} Flagging missing data'.format(datetime.now()-start_time))
    data = flag_missing_data(data,
                             probefield=probefield,
                             providerfield=providerfield,
                             timestampfield=timestampfield,
                             latfield=latfield, lonfield=lonfield)

    # Remove flagged data from the main dataset
    data, err_data = segregate_flagged_errors(data, err_data)

    nerr = len(err_data)
    print('{t} Flagged {n} rows; kept {m}'.format(
        t=datetime.now()-start_time, n=nerr, m=len(data)))

    # Flag duplicate timestamps
    print('{} Flagging duplicate timestamps'.format(datetime.now()-start_time))
    data = flag_duplicate_timestamps(data, tracefield='trace_id')
    # Segregate points that were flagged
    data, err_data = segregate_flagged_errors(data, err_data)
    derr = len(err_data) - nerr
    nerr = len(err_data)

    print('{t} Flagged {n} rows; kept {m}'.format(
        t=datetime.now()-start_time, n=derr, m=len(data)))

    # Flag singleton traces
    data, err_data = flag_singletons(data, err_data=err_data,
                                     start_time=start_time)


    if not recombine_data:
        return data, err_data

    # Combine good and bad data into a single data set
    data = pd.concat([data, err_data], sort=False)
    return data


def infer_dvh(data, start_time=None, err_data=None, tracefield='trace_id',
              timestampfield=TIMESTAMPFIELD, timestamp_precision=TIMESTAMP_PRECISION,
              latfield=LATFIELD, lonfield=LONFIELD,
              do_upstream=False, recombine_data=True, **kwargs):
    '''
    Calculates inferred "crow-flies" distance, velocity, and heading values 
    between subsequent data points. To ensure the best accuracy, this
    function then identifies "stuck location" errors, flags and segregates 
    them, and then recomputed the distance/velocity/heading values on the 
    remaining data, untill all such "stuck" points have been eliminated.

    Returns a dataset with new fields for distance, velocity, and heading
    from the previous point (see the crowflies module), as well as error
    flagging for stuck-location errors (and any upstream calculations if 
    do_upstream=True). If recombine_data=False, the "clean" and flagged
    datasets are returned in separate dataframes.

    Arguments:
    ----------
    A dataset containing GPS data for one or more probes

    Keyword Arguments:
    ------------------
    start_time: A timestamp representing the time at which the current 
        pipeline process started. If not provided, the current time will
        be used.
    err_data: a dataframe containing data that has been previously
        flagged as erroneous and segregated from the main dataset (if any).
    tracefield: The name of the field identifying unique traces within the 
        dataset
    timestampfield: The name of the field containing timestamp data for each 
        GPS point
    latfield, lonfield: The names of the fields containing latitude and 
        longitude data
    timestamp_precision: the reporting precision of the timestamp field
         (in seconds). If the timestamps are reported to millisecond 
         precision, set this to 0.001. Default is 1 second.
    do_upstream: if True, also do all upstream calculations that are necessary
        to prepare an unprocessed dataset for this step in the 
        pipeline (i.e., call flag_missing_and_duplicate)
    recombine_data: if True, return a single dataset, with errors flagged. 
        If False, return two datasets, the first one being "clean" and the
        second one containing all of the data that was flagged in this step.
    **kwargs: Additional keywords passed through to the upstream processing
        steps (flag_missing_and_duplicate)
    '''
    if not start_time:
        start_time = datetime.now()

    if do_upstream:  # Do upstream error-processing steps
        # Flag missing data and duplicate timestamps
        data, err_data = flag_missing_and_duplicate(
            data, err_data=err_data, tracefield='trace_id',
            timestampfield=TIMESTAMPFIELD, latfield=LATFIELD, lonfield=LONFIELD,
            start_time=start_time, do_upstream=True, recombine_data=False,
            **kwargs)

    if err_data is None:
        nerr = 0
    else:
        nerr = len(err_data)

    # Sort data for velocity and heading calcs
    print('{} Sorting data by trace ID and timestamp'.format(
        datetime.now()-start_time))
    data.sort_values(by=[tracefield, timestampfield], inplace=True)

    # Compute inferred distances and velocities.
    print('{} Computing distance and velocity'.format(
        datetime.now()-start_time))
    # Compute crow-flies distances and velocities for non-error points
    # This ends up making a copy of most of the data, which is not ideal.
    # There may be a smarter approach. The key issue is that the functions
    # below append new columns, so we can't operate on slices.
    data = compute_time_dist_avg_vel_all(
        data, is_sorted=True, tracefield=tracefield,
        timestampfield=timestampfield, latfield=latfield, lonfield=lonfield,
        timestamp_precision=timestamp_precision)

    # Flag data points with stuck-location errors and recompute d/v/h values
    print('{} Flagging stuck locations and recomputing velocities'.format(
        datetime.now()-start_time))
    data = flag_stuck_locations(data, is_sorted=True)
    # Segregate flagged points
    data, err_data = segregate_flagged_errors(data, err_data)
    derr = len(err_data) - nerr
    nerr = len(err_data)

    print('{t} Flagged {n} rows; kept {m}'.format(
        t=datetime.now()-start_time, n=derr, m=len(data)))

    # Flag singleton traces (again)
    data, err_data = flag_singletons(data, err_data=err_data,
                                     start_time=start_time)

    if not recombine_data:
        return data, err_data

    # Combine good and bad data into a single data set
    data = pd.concat([data, err_data], sort=False)
    return data


def compute_samprate_and_split(data, start_time=None, err_data=None,
                               tracefield='trace_id',
                               timestepfield='sec_since_prev',
                               timestamp_precision=TIMESTAMP_PRECISION,
                               lag_factor=LAG_FACTOR, min_lag=MIN_LAG,
                               do_upstream=False, recombine_data=True,
                               **kwargs):
    '''
    Calculates sampling rates for each individual trace, and splits up
    traces on long sampling lags.

    Returns a dataset with a new field for the sampling rate, as well as 
    updated trace IDs following the trace splitting. Any new singleton
    traces created in the splitting process are flagged as such in the 
    error_flag field (as are errors from any upstream calculations if 
    do_upstream=True). If recombine_data=False, the "clean" and flagged
    datasets are returned in separate dataframes.

    Arguments:
    ----------
    A dataset containing GPS data for one or more probes

    Keyword Arguments:
    ------------------
    start_time: A timestamp representing the time at which the current 
        pipeline process started. If not provided, the current time will
        be used.
    err_data: a dataframe containing data that has been previously
        flagged as erroneous and segregated from the main dataset (if any).
    tracefield: The name of the field identifying unique traces within the 
        dataset
    timestampfield: The name of the field containing timestamp data for each 
        GPS point
    timestamp_precision: the reporting precision of the timestamp field
         (in seconds). If the timestamps are reported to millisecond 
         precision, set this to 0.001. Default is 1 second.
    do_upstream: if True, also do all upstream calculations that are necessary
        to prepare an unprocessed dataset for this step in the 
        pipeline (i.e., call flag_missing_and_duplicate)
    is_sorted: If True, assume that the input data is sorted in ascending order
        by timestamp.
    recombine_data: if True, return a single dataset, with errors flagged. 
        If False, return two datasets, the first one being "clean" and the
        second one containing all of the data that was flagged in this step.
    **kwargs: Additional keywords passed through to the upstream processing
        steps (infer_dvh)
    '''
    if not start_time:
        start_time = datetime.now()

    if do_upstream:  # Do upstream error-processing steps
        # Run infer_dvh and all upstream processes
        data, err_data = infer_dvh(
            data, start_time=start_time, err_data=err_data,
            tracefield=tracefield,
            timestepfield=timestepfield,
            timestamp_precision=timestamp_precision,
            do_upstream=True, recombine_data=False, **kwargs)

    # Compute sampling rates
    print('{} Computing sampling rates for each trace'.format(
        datetime.now()-start_time))
    data = data.groupby(tracefield).apply(
        get_sampling_period,
        timestepfield=timestepfield,
        timestamp_precision=timestamp_precision)

    ntraces0 = data.trace_id.nunique()

    print('{} Splitting traces on long time lag (and recalculating sampling rates)'.format(
        datetime.now()-start_time))
    data = split_on_large_lag(
        data, timestepfield=timestepfield, lag_factor=lag_factor,
        min_lag=min_lag, tracefield=tracefield)
    print(
        '{t} Splitting created {n} new traces from an original set of {m}.'.format(
            t=datetime.now()-start_time, n=data.trace_id.nunique()-ntraces0,
            m=ntraces0))

    # flag singletons yet again
    data, err_data = flag_singletons(data, err_data=err_data,
                                     start_time=start_time)

    # Compute inferred heading changes for good data
    print('{} Computing heading changes'.format(datetime.now()-start_time))
    data = compute_abs_head_ch_all(data)

    if not recombine_data:
        return data, err_data

    # Combine good and bad data into a single data set
    data = pd.concat([data, err_data], sort=False)
    return data


def flag_anomalous_points(data, start_time=None, err_data=None,
                          tracefield='trace_id', timestampfield=TIMESTAMPFIELD,
                          timestamp_precision=TIMESTAMP_PRECISION,
                          velocity_thresh=VELOCITY_THRESH, heading_change_thresh=HEADING_CHANGE_THRESH,
                          anom_frac_thresh=ANOM_FRAC_THRESH,
                          is_sorted=False,
                          do_upstream=False, **kwargs):
    '''
    Calculates inferred one point and two-point absolute heading changes 
    for each point, then flags all velocity, acute position, and sampling rate 
    outliers.

    Arguments:
    ----------
    data: A dataset containing GPS data for one or more probes

    Keyword Arguments:
    ------------------
    start_time: A timestamp representing the time at which the current 
        pipeline process started. If not provided, the current time will
        be used.
    err_data: a dataframe containing data that has been previously
        flagged as erroneous and segregated from the main dataset (if any).
    tracefield: The name of the field identifying unique traces within the 
        dataset
    timestampfield: The name of the field containing timestamp data for each 
        GPS point
    timestamp_precision: the reporting precision of the timestamp field
         (in seconds). If the timestamps are reported to millisecond 
         precision, set this to 0.001. Default is 1 second.
    velocity_thresh: The threshold above which to exclude points as velocity
        outliers.
    heading_change_thresh: The threshold above which to exlude points as acute
        position outliers, based on their two-point absolute heading change.
    anom_frac_thresh: The threshold above which to flag entire traces that
    do_upstream: if True, also do all upstream calculations that are necessary
        to prepare an unprocessed dataset for this step in the 
        pipeline (i.e., call flag_missing_and_duplicate)
    **kwargs: Additional keywords passed through to the upstream processing
        steps (infer_dvh)
    '''

    if not start_time:
        start_time = datetime.now()

    if do_upstream:  # Do upstream error-processing steps
        # Run infer_dvh and all upstream processes
        data, err_data = compute_samprate_and_split(
            data, start_time=start_time, err_data=err_data,
            do_upstream=True, recombine_data=False, **kwargs)

    # Recombine good and bad data into a single data set
    if err_data is not None:
        data = pd.concat([data, err_data], sort=False)

    # Flag velocity outliers
    print('{} Flagging velocity outliers.'.format(
        datetime.now()-start_time))
    data = flag_velocity_outliers(data, threshold=velocity_thresh)
    nerr = ((data['error_flag'] & 2**ERROR_FLAGS['velocity']) > 0).sum()

    print('{t} Flagged {n} rows'.format(
        t=datetime.now()-start_time, n=nerr))

    # flag acute position outliers
    print('{} Flagging acute position outliers.'.format(
        datetime.now()-start_time))
    data = flag_acute_position_outliers(data, threshold=heading_change_thresh)
    nerr = ((data['error_flag'] & 2**ERROR_FLAGS['position_acute']) > 0).sum()

    print('{t} Flagged {n} rows'.format(
        t=datetime.now()-start_time, n=nerr))

    # Flag sampling rate anomalies
    print('{} Flagging sampling rate anomalies.'.format(
        datetime.now()-start_time))
    data = flag_sample_period_anomalies(data,
                                      timestamp_precision=timestamp_precision)
    nerr = ((data['error_flag'] & 2**ERROR_FLAGS['sampling']) > 0).sum()

    print('{t} Flagged {n} rows'.format(
        t=datetime.now()-start_time, n=nerr))

    # Flag traces that have excessive anomaly fractions
    print('{} Flagging traces having excessive pos/vel anomalies.'.format(
        datetime.now()-start_time))
    data = flag_excessive_anomaly_fractions(data,
                                            tracefield=tracefield,
                                            threshold=anom_frac_thresh)
    nerr = ((data['error_flag'] & 2 **
             ERROR_FLAGS['anomaly_fraction']) > 0).sum()

    print('{t} Flagged {n} rows'.format(
        t=datetime.now()-start_time, n=nerr))

    return data


def full_pipe(data, start_time=None, print_output=False):
    '''

    This function runs the full error_flagging pipeline. It is primarily a
    helper function for the flag_simple_errors.py script. It carries out the 
    following steps:
    *  Identifies initial traces as unique combinations of probe ID and 
    provider ID.
    *  Identifies and flags any points that are missing data in key fields, 
    and excludes these from further consideration
    *  Identifies and flags any points with non-unique timestamps within each 
    trace, and excludes all but one of each from further consideration
    *  Calculates time lags between points, and inferred as-the-crow-flies 
    distances, velocities, and headings.
    *  Identifies and flags points whose location appears to be "stuck" (i.e., 
    not updated at all from one point to the next), excludes these from 
    further consideration, and recomputes distances, velocities, and headings 
    as needed.
    *  Identifies and flags singleton traces (traces that consist of only one 
    point that has not been flagged and excluded), and excludes these from 
    further consideration. (This step is repeated a few times downstream as 
    well)
    *  Estimates a sampling rate for each trace (we can discuss this algorithm 
    in more detail on Monday)
    *  Splits up traces where there are unusually long time lags between points 
    (currently splitting on lags that are more than 10x the sampling rate AND 
    more than 600 seconds).
    *  Computes the change in heading at each point, and integrated over each 
    sequential pair of points, to identify "backtracking" type behavior
    *  Identifies and flags stationary points (i.e., points where the probe
    appears to be at rest).
    *  Flags points with unreasonably high velocities
    *  Flags points that appear to be exhibiting unrealistic backtracking/zig-zag 
    behavior, which often indicates a position outlier of the sort that exhibits 
    an acute angle
    *  Flags points that appear to be inconsistent with their trace's sampling 
    rate (all points are flagged if  we were unable to determine a sampling 
    rate for a given trace)
    *  Flags all points in traces for which >50% of points appear to be either 
    velocity or position outliers
    '''

    # Do data processing in one processing to ease parallelization.
    if not print_output:
        sys.stdout = open(os.devnull, 'w')
    data = create_unique_trace_ids(data)
    data, err_data = flag_missing_and_duplicate(data, start_time=start_time,
                                                recombine_data=False)
    data, err_data = infer_dvh(data, start_time=start_time, err_data=err_data,
                               recombine_data=False)

    data, err_data = compute_samprate_and_split(data,start_time=start_time,
                                                recombine_data=False)
    # do stationary point detection
    print('{} Flagging stationary points'.format(datetime.now()-start_time))
    data = find_stationary_points(data)
    print('{t} Flagged {n} rows in stationary points'.format(
        t=datetime.now()-start_time,
        n=(data['stationary_point_flag'] > 0).sum()))
    # flag anomalous
    data = flag_anomalous_points(data,start_time=start_time,
                                 err_data=err_data, is_sorted=True)
    if not print_output:
        sys.stdout = sys.__stdout__
    return data

