#!/usr/bin/env python3
# (the above line lets us run this at the Linux command line)
import sys
import os
import multiprocessing
import ast
from datetime import datetime
import pandas as pd
import numpy as np
from traverse.trace_processing.identify import (
    create_unique_trace_ids)
from traverse.pipeline.processing import (
    flag_missing_and_duplicate, infer_dvh, compute_samprate_and_split,
    flag_anomalous_points, full_pipe)
from traverse.utils.misc import get_date_columns, parallel_by_group
from traverse.trace_processing.stationary import (
    find_stationary_points, stationary_loop, calc_dur, continuity_chop)
from traverse.settings import TIMESTAMP_KEYWORDS, THREAD_COUNT
                               
def run(infile, outfile=None, thread_count=THREAD_COUNT, 
        verbose=True, return_dataframe=False):
    
    '''
    This function does a full end-to-end run of the simple error-flagging pipeline:
    *  Identifies initial traces as unique combinations of probe ID and
    provider ID.
    *  Identifies and flags any points that are missing data in key fields,
    and excludes these from further consideration
    *  Identifies and flags any points with non-unique timestamps within each
    trace, and excludes all but one of each from further consideration
    *  Calculates time lags between points, and inferred as-the-crow-flies
    distances, velocities, and headings.
    *  Identifies and flags points whose location appears to be "stuck" (i.e.,
    not updated at all from one point to the next), excludes these from
    further consideration, and recomputes distances, velocities, and headings
    as needed.
    *  Identifies and flags singleton traces (traces that consist of only one
    point that has not been flagged and excluded), and excludes these from
    further consideration. (This step is repeated a few times downstream as
    well)
    *  Estimates a sampling rate for each trace (we can discuss this algorithm
    in more detail on Monday)
    *  Splits up traces where there are unusually long time lags between points
    (currently splitting on lags that are more than 10x the sampling rate AND
    more than 600 seconds).
    *  Computes the change in heading at each point, and integrated over each
    sequential pair of points, to identify "backtracking" type behavior
    *  Identifies and flags stationary points (i.e., points where the probe
    appears to be at rest).
    *  Flags points with unreasonably high velocities
    *  Flags points that appear to be exhibiting unrealistic backtracking/zig-zag
    behavior, which often indicates a position outlier of the sort that exhibits
    an acute angle
    *  Flags points that appear to be inconsistent with their trace's sampling
    rate (all points are flagged if  we were unable to determine a sampling
    rate for a given trace)
    *  Flags all points in traces for which >50% of points appear to be either
    velocity or position outliers

    Arguments:
    ----------
    infile: Path to your input data.

    Keyword Arguments:
    ------------------
    outfile: Path to the file where output data should be written. 
        If None, the input file name is used, with a suffix. 
    thread_count: Number of thread to use for parallel processing. Should
        be set <= the number of processors available. 
    verbose: If True, print out progress statements.
    return_dataframe: If True, return the flagged dataset as a dataframe, 
        in addition to writing it out.

    To run this at the command line, you can do (from within the pipeline directory)
    $ python3 flag_simple_errors input_file.csv [output_file.csv] [num_processors]
    '''
    start_time = datetime.now()

    if not verbose:
        sys.stdout = open(os.devnull, 'w')
    if outfile == None:
        outfile = infile[:-4] + '_flagged.csv'
    #run the pipeline
    print('{} Reading input file'.format(datetime.now() - start_time))
    data = pd.read_csv(infile, 
                       parse_dates=get_date_columns(infile, TIMESTAMP_KEYWORDS))
    data.columns = [c.lower() for c in data.columns]
    print('{t} Processing {n} rows of data.'.format(
        t=datetime.now() - start_time, n=len(data)))
    if thread_count == 1:
        data = full_pipe(data, start_time,
                         print_output=True)
    else:
        try:
            ret_list = parallel_by_group(data, thread_count)
        except KeyError:
            print('Please make sure you have modified the settings'+
                  ' to match your dataset.')
            sys.exit() 
        print('Splitting across {th} threads'.format(th=thread_count))
        print(
            'The output statement will refer to the first thread, '+
            'processing {n} rows of data.'.format(
                n=len(ret_list[0])))
        # make a list to print out some timing info
        print_list = np.repeat(False, thread_count)
        if verbose:
            print_list[0] = True
        # run pipeline
        with multiprocessing.Pool(thread_count) as p:
            # do data processing
            ret_list = p.starmap(full_pipe,
                                 [(ret_list[i],
                                   start_time,
                                   print_list[i])
                                  for i in range(thread_count)])
        data = pd.concat(ret_list)
    # Write output file.
    print('{} Writing out.'.format(datetime.now() - start_time))
    data.to_csv(outfile, index=False)
    print('{} Done.'.format(datetime.now() - start_time))
    #return printing ability
    if not verbose:
        sys.stdout = sys.__stdout__
    if return_dataframe:
        return data


#For runnning at the command line:
if __name__ == '__main__':


    # Determine input and output filenames
    infile = sys.argv[1]
    try:
        outfile = sys.argv[2]
        if str.isnumeric(outfile):
            thread_count = ast.literal_eval(outfile)
            if thread_count > multiprocessing.cpu_count():
                print('your thread count exceeds your number of processors (' +
                      str(multiprocessing.cpu_count()) + 
                      '). Defaulting to settings value')
                thread_count = THREAD_COUNT
            outfile = infile[:-4] + '_flagged.csv'
    except IndexError:
        outfile = infile[:-4] + '_flagged.csv'
    except BaseException:
        print('Please provide an output file name and an integer thread count.')
        raise

    if 'thread_count' not in locals():
        try:
            thread_count = ast.literal_eval(sys.argv[3])
            # make sure it's numeric
            if not isinstance(thread_count, int):
                print('thread count must be an integer')
                sys.exit(1)
            if thread_count > multiprocessing.cpu_count():
                print('your thread count exceeds your number of processors (' +
                      str(multiprocessing.cpu_count()) + 
                      '). This may slow things down.')
        except BaseException:
            thread_count = THREAD_COUNT
            print('No usable thread count provided. Defaulting to settings value.')

    #Read in data run the pipeline
    run(infile, outfile=outfile, thread_count=thread_count)
