#!/usr/bin/env python3
#(the above line lets us run this at the Linux command line)
import sys
from datetime import datetime
import pandas as pd
from traverse.pipeline.processing import infer_dvh
from traverse.utils.misc import get_date_columns

'''
This script does a partial run of the error flagging pipeline, up to 
the point of computing the inferred distance, velocity, and heading values,
without flagging data anomalies or singleton traces.

To run this at the command line, you can do
$ python infer_dvh input_file.csv [output_file.csv]
'''

if __name__ == '__main__':
    start_time = datetime.now()
    
    #Determine input and output filenames
    infile = sys.argv[1]
    try:
        outfile = sys.argv[2]
    except IndexError:
        outfile = infile[:-4]+'_with_dvh.csv'

    
    #Read in data
    print('{} Reading input file'.format(datetime.now()-start_time))
    data = pd.read_csv(infile, parse_dates=get_date_columns(infile))
    data.columns = [c.lower() for c in data.columns]
    print('{t} Processing {n} rows of data.'.format(
        t=datetime.now()-start_time, n=len(data)))

    #Do data processing
    data = infer_dvh(data, start_time, do_upstream=True)

    #Write output file.
    print('{} Writing out.'.format(datetime.now()-start_time))
    data.to_csv(outfile)
    print('{} Done.'.format(datetime.now()-start_time))