import pandas as pd
import numpy as np 
from ..trace_processing.crowflies import compute_time_dist_avg_vel_all
from traverse.settings import *

#Define the flagging scheme for different error types
ERROR_FLAGS = {'missing': 10, #Missing data
               'duplicate': 9, #Duplicate timestamps
               'stuck': 8, #Stuck location
               'placeholder1': 7, #Unused
               'singleton': 6, #Singleton trace
               'anomaly_fraction': 5, #High fraction of anomalies in trace
               'placeholder2': 4, #Unused
               'velocity': 3, #Velocity outlier
               'position_acute': 2, #Position outlier with acute angle
               'position_shallow': 1, #Position outlier with shallow angle
               'sampling': 0} #Sampling rate anomaly
               
def flag_missing_data(data, 
                      probefield=PROBEFIELD,
                      providerfield=PROVIDERFIELD, 
                      timestampfield=TIMESTAMPFIELD,
                      latfield=LATFIELD, lonfield=LONFIELD):
    '''
    Flag rows of data that are missing information in certain
    key columns. Errors of this sort are flagged with an error flag of 
    2**10.  If all data comes from the same provider, can
    set providerfield to None to ignore this field.

    Arguments:
    ----------
    data: A GPS dataset containing column for unique probe ID, 
        data provider ID (optional), timestamp, latitude, and longitude.

    Keywords:
    ---------
    probefield: The name of the column containing the unique probe ID.
    providerfield: The name of the column containing the provider ID. 
        If all data is known to come from the same provider (i.e., if you 
        are sure that all probe IDs are fully unique), this argument can be 
        set to None skip consideration of this column.
    timestampfield: The name of the field containing timestamp information.
    latfield, lonfield: The names of the fields containing lat/lon information.
    '''
    flag = 2**ERROR_FLAGS['missing']

    if not ('error_flag' in data.columns):
        data['error_flag'] = 0
    
    try:
        data.loc[data[probefield].isna()|(data[probefield]==None), 
                'error_flag'] |= flag
        if providerfield:
            data.loc[data[providerfield].isna()|(data[providerfield]==None), 
                    'error_flag'] |= flag
        data.loc[data[timestampfield].isna()|(data[timestampfield]==None), 
                'error_flag'] |= flag
        data.loc[data[latfield].isna(), 'error_flag'] |= flag
        data.loc[data[lonfield].isna(), 'error_flag'] |= flag
    except(KeyError):
        print('One of the field names provided in flag_missing_data'+
              ' was not present in the dataset. Please pass the appropriate'+
              ' field names as keyword arguments.')
        raise

    return data

def flag_duplicate_timestamps(
    data, tracefield='trace_id', timestampfield=TIMESTAMPFIELD):
    '''
    Flag rows of data that have timestamps that are identical to a timestamp
    that has already been found within a given trace. The resulting data
    will have error_flag 2**9 for all duplicate timestamps except for one.

    Arguments:
    ----------
    data: A GPS dataset containing column for unique probe ID, 
        data provider ID (optional), timestamp, latitude, and longitude.

    Keywords:
    ---------
    tracefield: The name of the column representing a unique trace ID
    timestampfield: The name of the field containing timestamp information.
    '''
    flag = 2**ERROR_FLAGS['duplicate']
    #Reset the index so that we can use the original unique index as a
    #data field
    data.reset_index(inplace=True)
    #Get the unique time stamp values per probe trajectory
    unique_timestamps = data.groupby(
        [tracefield, timestampfield], sort=False).first().reset_index()

    #Flag any duplicate timestamps (except for the first one of each set)
    if not ('error_flag' in data.columns):
        data['error_flag'] = 0 
    data.loc[~np.isin(data['index'], unique_timestamps['index']),
             'error_flag'] |= flag

    #Put back the original index
    data.set_index('index',inplace=True)
    
    return data



def flag_stuck_locations(data, **kwargs):
    '''
    Flag rows of data whose position is exactly identical to the previous
    timestamp, indicating that the sample failed to update the position.
    The resulting data will get error_flag 2**8 for all such 
    points. 

    This function also calls crowflies.compute_time_dist_avg_vel_all() on
    the resulting data set to recompute distances and velocities on the 
    unflagged data, since the flagged points will lead to incorrect inferred
    velocities based on their incorrect location data.

    Arguments:
    ----------
    data: A GPS dataset containing column for unique probe ID, 
        data provider ID (optional), timestamp, latitude, and longitude.

    Keywords:
    ---------
    **kwargs: Keyword arguments for crowflies.compute_time_dist_avg_vel_all.
    '''
    flag = 2**ERROR_FLAGS['stuck']

    if not ('error_flag' in data.columns):
        data['error_flag'] = 0

    try:
        bad = ((data['sec_since_prev']>0) & 
               (data['dist_from_prev']==0) & 
               (data['error_flag']==0))
    except KeyError:
        data.loc[data['error_flag']==0,:] = compute_time_dist_avg_vel_all(
            data.loc[data['error_flag']==0,:], **kwargs)
        bad = ((data['sec_since_prev']>0) & 
               (data['dist_from_prev']==0) & 
               (data['error_flag']==0))

    while bad.sum() > 0:
        data.loc[bad, 'error_flag'] |= flag
        data.loc[bad, ['dist_from_prev', 
                       'sec_since_prev',
                       'avg_vel_from_prev',
                       'min_avg_vel_from_prev',
                       'heading_from_prev']] = 0

        data.loc[data['error_flag']==0,:] = compute_time_dist_avg_vel_all(
            data.loc[data['error_flag']==0,:].copy(), **kwargs)
        bad = ((data['sec_since_prev']>0) & 
               (data['dist_from_prev']==0) & 
               (data['error_flag']==0))
    
    return data


def flag_singleton_traces(data, tracefield='trace_id'):
    '''
    Flag singleton traces, i.e., traces that consist of only a single data
    point. The resulting data will get error_flag 2**6 for all 
    such points. 

    Arguments:
    ----------
    data: A GPS dataset containing column for unique probe ID, 
        data provider ID (optional), timestamp, latitude, and longitude.

    Keywords:
    ---------
    tracefield: The name of the column representing a unique trace ID
    timestampfield: The name of the field containing timestamp information.
    '''
    flag = 2**ERROR_FLAGS['singleton']

    if not ('error_flag' in data.columns):
        data['error_flag'] = 0

    trace_lengths = data[tracefield].value_counts()

    singletons = trace_lengths[trace_lengths==1].index.values

    data.loc[data[tracefield].isin(singletons), 'error_flag'] |= flag

    return data

def flag_velocity_outliers(data, threshold=160,
                           velocityfield='min_avg_vel_from_prev'):
    '''
    Flag points that have unrealistically high velocities (above the
    specified threshold). 

    Arguments:
    ----------
    data: A GPS dataset containing columns specifying the velocity from the 
        previous point.

    Keywords:
    ---------
    velocity_thresh: The threshold value above which to flag outliers.
    velocityfield: The name of the column containing the velocity data.

    '''
    flag = 2**ERROR_FLAGS['velocity']

    if not ('error_flag' in data.columns):
        data['error_flag'] = 0

    data.loc[data[velocityfield]>threshold, 
             'error_flag'] |= flag
    
    return data

def flag_acute_position_outliers(data, threshold = 270):
    '''
    This function checks the absolute heading change over
    two points. If the value is extreme a flag is assigned. 
    Data must be preprocessed through the crowflies module
     to compute the heading changes
    
    Arguments:
    ----------
    data: A GPS dataset containing column for unique probe ID, 
        data provider ID (optional), timestamp, latitude, and longitude

    Keywords:
    ---------
    threshold: the total change in heading over 
                two points at which to raise a flag

    '''
    flag = 2**ERROR_FLAGS['position_acute']

    if not ('error_flag' in data.columns):
        data['error_flag'] = 0

    try:
        bad = ((data['abs_heading_change_last_2']>threshold) & 
               (data['stationary_point_flag'] <0))
    except KeyError:
        print("Please make sure you have preprocessed the data to generate"+
              " two-point heading changes and stationary point flags.")
        raise
    
    data.loc[bad,'error_flag'] |= flag
    return data

def flag_sample_period_anomalies(data, timestepfield='sec_since_prev', 
                               timestamp_precision=1):
    '''
    Flag rows of data where the sampling rate does not match 
    the detected sampling rate. Traces for which a sampling rate
    could not be determined will *all* be flagged as anomalies.
    Must be run after sampling rate detection. 
    
    Arguments:
    ----------
    data: A GPS dataset containing column for unique probe ID, 
        data provider ID (optional), timestamp, latitude, and longitude.

    Keywords:
    ---------
    timestepfield: Name of the field containing the seconds between each 
        sample.
    timestamp_precision: the timestamp precision. Used as allowable error.
    
    '''
    
    flag = 2**ERROR_FLAGS['sampling']
    
    if not ('error_flag' in data.columns):
        data['error_flag'] = 0

    #Find points with bad data that will not have had sampling rates
    #estimated. Don't want to flag these as sampling rate anomalies.
    bad = data['error_flag'] >= 2**ERROR_FLAGS['stuck']

    sr = data['sample_period']

    ts = data[timestepfield]
    #mark the anomalous rates
    norm_rate = (ts % sr <= timestamp_precision) | (
                abs(ts % sr - sr) <= timestamp_precision)
   
    data.loc[(~norm_rate) & (~bad), 'error_flag'] |= flag
    
    return data

def _flag_excessive_flag_fraction(trace_flags, test_flag, thresh, flag):
    '''
    Flag an entire trace if it has an excessive fraction of error flags
    matching a given test flag value. Used as a helper function in
    flag_excessive_anomaly_fractions.

    Arguments:
    ----------
    trace_flags: numpy array representing the error flags for a trace
    test_flag: Compare all flags to this flag. If the fraction of flags 
        matching this flag is greater than thresh, flag the entire trace.
    threshold: Threshold fraction above which to flag the entire trace.
    flag: Flag value with which to flag problematic traces.
    '''
    frac = ((trace_flags & test_flag)>0).sum()/float(len(trace_flags))

    if frac > thresh:
        trace_flags |= flag

    return trace_flags


def flag_excessive_anomaly_fractions(data, threshold=0.5,
                                     tracefield='trace_id'):
    '''
    Flag traces that have an excessive fraction of velocity and position
    anomalies.

    Arguments
    ---------
    data: A GPS dataset containing column for unique probe ID, 
        data provider ID (optional), timestamp, latitude, and longitude.

    Keywords:
    ---------
    threshold: The anomaly fraction above which to flag a trace as 
        problematic.
    tracefield: Field name containing the unique trace ID.
    '''
    flag = 2**ERROR_FLAGS['anomaly_fraction']

    test_flag = ((2**ERROR_FLAGS['velocity']) |
                 (2**ERROR_FLAGS['position_acute']) |
                 (2**ERROR_FLAGS['position_shallow']))

    #to save time, only pass data that contains one or more of these errors
    er_traces = data.loc[
        (data['error_flag'] & test_flag)>0, tracefield].unique()
    dat = data.loc[data[tracefield].isin(er_traces)].copy()
        #(explicitly make a copy above to avoid annoying pandas warning)

    #Flag traces with excessive anomaly fractions                  
    dat.loc[:,'error_flag'] = dat.fillna({tracefield:''}).groupby(
        tracefield)['error_flag'].transform(
        _flag_excessive_flag_fraction, test_flag, threshold, flag
    )

    data.loc[data[tracefield].isin(er_traces),'error_flag'] = \
        dat['error_flag'].values
    return data

def segregate_flagged_errors(data, err_data=None):
    '''
    Remove points with flagged errors from the main dataset and add
    them to an auxiliary dataset containing only error points.

    Returns the dataset with error points removed, and the error set
    with the error points appended.

    Arguments:
    ----------
    data: the dataset with flagged errors

    Keyword Arguments:
    ------------------
    err_data: the auxiliary dataset of flagged errors
    '''
    #Get the subset of the data that has a nonzero error flag.
    err = data['error_flag']!=0
    if err_data is None:
        #Save it as a separate dataframe if none already exists.
        err_data = data.loc[err].copy()
    else:
        #Otherwise glue it together with the existing error data.
        err_data = pd.concat([err_data, data.loc[err].copy()], sort=False)

    #Now remove the flagged data from the main dataset.
    data = data.loc[~err].copy()
    
    return data, err_data

