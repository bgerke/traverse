import pandas as pd 
import numpy as np
import pytest
from ..error_flagging import (
    flag_missing_data, flag_duplicate_timestamps, 
    flag_singleton_traces, flag_stuck_locations, 
    flag_velocity_outliers, flag_sample_period_anomalies,
    flag_acute_position_outliers, flag_excessive_anomaly_fractions,
    ERROR_FLAGS)
from ...trace_processing.sampling_period import get_sampling_period

def test_flag_missing_data():
    '''
    Test that expected missing data is flagged
    '''
    flag = 2**ERROR_FLAGS['missing']
    data = pd.DataFrame(
        {'probe_id':[None, np.nan, 1,1,1,1,1,1],
        'probe_data_provider':[1,1,np.nan,None,1,1,1,1],
        'sample_date':['1','2','3','4',None,np.nan,'1','1'],
        'lat':[1.,2.,3.,4.,5.,6.,np.nan, 8.],
        'lon':[1.,2.,3.,4.,5.,6.,7.,np.nan]})
    data = flag_missing_data(data)
    assert data['error_flag'].sum() == 8*flag

def test_flag_missing_data_no_providerfield():
    '''
    Test that we can skip the provider field when flagging missing data
    '''
    flag = 2**ERROR_FLAGS['missing']
    data = pd.DataFrame(
        {'probe_id':[None, np.nan, 1,1,1,1,1,1],
        #'probe_data_provider':[1,1,np.nan,None,1,1,1,1],
        'sample_date':['1','2','3','4',None,np.nan,'1','1'],
        'lat':[1.,2.,3.,4.,5.,6.,np.nan, 8.],
        'lon':[1.,2.,3.,4.,5.,6.,7.,np.nan]})
    data = flag_missing_data(data, providerfield=None)
    assert data['error_flag'].sum() == 6*flag
    
def test_flag_missing_data_keyerror():
    '''
    Test that we get a KeyError if the field names are wrong in 
    flag_missing_data
    '''
    data = pd.DataFrame(
        {'trace_id':[None, np.nan, 1,1,1,1,1,1],
        #'probe_data_provider':[1,1,np.nan,None,1,1,1,1],
        'sample_date':['1','2','3','4',None,np.nan,'1','1'],
        'lat':[1.,2.,3.,4.,5.,6.,np.nan, 8.],
        'lon':[1.,2.,3.,4.,5.,6.,7.,np.nan]})
    with pytest.raises(KeyError):
        data = flag_missing_data(data)

def test_flag_duplicate_timestamps():
    '''
    Should flag duplicate timestamp values with the same probe id, but not
    with different ones
    '''
    flag = 2**ERROR_FLAGS['duplicate']

    data = pd.DataFrame(
        {'trace_id':[1,1,1,2,2,2],
         'sample_date':[1200,1200,1202, 1200,1201,1201]})
    
    data = flag_duplicate_timestamps(data)
    assert list(data['error_flag'].values) == [0,flag,0,0,0,flag]

def test_flag_duplicate_timestamps_alternate_column_names():
    '''
    Can run flag_duplicate_timestamps with alternate column names
    '''

    flag = 2**ERROR_FLAGS['duplicate']
    data = pd.DataFrame(
        {'foo':[1,1,1,2,2,2],
         'bar':[1200,1200,1202, 1159,1159,1200]})
    
    data = flag_duplicate_timestamps(
        data, tracefield='foo',timestampfield='bar')
    assert list(data['error_flag'].values) == [0,flag,0,0,flag,0]

def test_flag_singleton_traces():
    '''
    can find singleton traces
    '''
    flag = 2**ERROR_FLAGS['singleton']
    data = pd.DataFrame(
        {'trace_id':[1,1,2,1,1,3,1,3]}
    )
    data = flag_singleton_traces(data)
    assert list(data['error_flag'].values) == [0,0,flag,0,0,0,0,0]

def test_flag_stuck_locations():
    '''
    Can flag a simple case of stuck location
    '''
    flag = 2**ERROR_FLAGS['stuck']
    npts = 5
    dates = pd.date_range('01-01-2018', periods=npts, freq='min').values
    lats = np.random.rand(npts)+np.random.rand(1)[0]*89*2-89
    lons = np.random.rand(npts)+np.random.rand(1)[0]*179*2-179
    data = pd.DataFrame(
        {'trace_id':[1,1,1,1,1],
         'sample_date':dates, 
         'lat':lats,
         'lon':lons,
         'sec_since_prev':[-1,5,5,0,5],
         'dist_from_prev': [0,1,1,0,0],
         'avg_vel_from_prev': [1,1,1,1,1],
         'min_avg_vel_from_prev':[1,1,1,1,1],
         'heading_from_prev':[0,0,0,0,0],
         'error_flag':[0,0,0,0,0]}
    )

    data = flag_stuck_locations(data)
    assert list(data['error_flag'].values) == [0,0,0,0,flag]

    #Should also have recomputed distances
    assert data['dist_from_prev'][3] > 0


def test_flag_velocity_outliers():
    '''
    Velocity outliers are flagged as expected
    '''
    flag = 2**ERROR_FLAGS['velocity']
    data = pd.DataFrame({'trace_id':[1,1,1,1,1], 
                         'velocity':[60.,70.,110.,170.,50.]})

    data = flag_velocity_outliers(data, velocityfield='velocity')
    #Default should flag everything above 160
    assert list(data['error_flag'].values) == \
        [0,0,0,flag,0]

    data = flag_velocity_outliers(data, threshold=100, 
                                  velocityfield='velocity')
    #Now should flag everything above 100
    assert list(data['error_flag'].values) == \
        [0,0,flag,flag,0]

def test_samp_anom_detect():
    '''
    check anomalous sample rates are flagged
    '''
    npts = 40
    A = np.repeat(15,30) 
    B = np.repeat(27,5)
    C = np.repeat(30,5) 
    probe = np.repeat('probe',npts)
    rates = np.concatenate((A,B,C))
    data = pd.DataFrame({'sec_since_prev':rates, 'trace_id':probe, 'trace_id':probe})
    data = get_sampling_period(data)   
    data = flag_sample_period_anomalies(data)
    assert(all(data.loc[30:34, 'error_flag']==2**ERROR_FLAGS['sampling']))
    
def test_samp_anom_detect_with_no_sr():
    '''
    check anomalous sample rates are flagged
    '''
    npts = 15
    A = np.repeat(15,5) 
    B = np.repeat(27,5)
    C = np.repeat(33,5) 
    probe = np.repeat('probe',npts)
    rates = np.concatenate((A,B,C))
    data = pd.DataFrame({'sec_since_prev':rates, 'trace_id':probe, 'trace_id':probe})
    data = get_sampling_period(data)   
    data = flag_sample_period_anomalies(data)
    
    assert(all(data[ 'error_flag']==2**ERROR_FLAGS['sampling']))
    
def test_acute_position_flag():
    
    '''
    flags a heading change greater than the threshold
    '''
    
    data = pd.DataFrame(
        {'trace_id':[1,1,1,1,1],
         'abs_heading_change_last_2':[10,15,300,300,15], 
         'error_flag':[0,0,0,0,0],
         'stationary_point_flag':[-1,-1,1,-1,-1]}
    )
    
    #compute heading change
    data = flag_acute_position_outliers(data)
    
    assert data.loc[3,'error_flag']==2**ERROR_FLAGS['position_acute']
    assert data.loc[2,'error_flag']==0

def test_excessive_anomaly_flagging():
    '''
    Can flag entire traces that have excessive anomaly flags
    '''
    f1 = 2**ERROR_FLAGS['velocity']
    f2 = 2**ERROR_FLAGS['position_acute']
    f0 = 2**ERROR_FLAGS['sampling']

    flag = 2**ERROR_FLAGS['anomaly_fraction']

    data=pd.DataFrame({'trace_id': [1,1,1,1,1,2,2,2,2,2], 
                       'error_flag': [f1,f1,f2,0,0,f1,f2,f0,f0,f0]},
                       index=['a','b','c','d','e','f','g','h','i','j'] 
                       #Set string index to make sure we're not implicitly
                       # depending on having an integer index 
    )
    
    data2 = flag_excessive_anomaly_fractions(data.copy()) 
                                #Make a copy so as not to overwrite flags
    #Should have flagged the first trace but not the second
    assert list(data2['error_flag'].values) == \
        [f1|flag, f1|flag, f2|flag, flag, flag, f1, f2, f0, f0, f0]

    data2 = flag_excessive_anomaly_fractions(data.copy(), threshold=0.2)
    #Should have flagged everything
    assert list(data2['error_flag'].values) == \
        [f1|flag, f1|flag, f2|flag, flag, flag, 
         f1|flag, f2|flag, f0|flag, f0|flag, f0|flag]

    data2 = flag_excessive_anomaly_fractions(data.copy(), threshold=0.9)
    #Should have flagged nothing
    assert list(data2['error_flag'].values) == \
        [f1, f1, f2, 0, 0, f1, f2, f0, f0, f0]

def test_excessive_anomaly_flagging_with_missing_data():
    '''
    Flagging excessive anomaly traces should not overwrite existing
    flags if there is missing data
    '''
    f1 = 2**ERROR_FLAGS['velocity']
    f2 = 2**ERROR_FLAGS['position_acute']
    f0 = 2**ERROR_FLAGS['sampling']
    
    flag = 2**ERROR_FLAGS['anomaly_fraction']

    data=pd.DataFrame({'trace_id': [1,1,1,1,1,2,2,2,2,2], 
                       'error_flag': [f1,f1,f2,0,0,f1,f2,f0,f0,f0]}
    )

    #Add on some points that were flagged upstream for missing data
    #They don't have trace IDs.
    data = data.append(pd.DataFrame({'error_flag':[2**10, 2**10]}), sort=False)

    print(data)

    data = flag_excessive_anomaly_fractions(data)

    #There should not be any NA values
    assert (~data['error_flag'].isna()).all()

    #Values should be what we expect
    print(data)
    assert list(data['error_flag'].values) == \
        [f1|flag, f1|flag, f2|flag, flag, flag, 
         f1, f2, f0, f0, f0, 2**10, 2**10]