#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='traverse',
      version='1.0.0',
      description='Data Veracity tools for GPS trajectory data',
      author=['Brian Gerke','Joe Glass-Katz'],
      author_email='bfgerke@lbl.gov',
      install_requires=['pandas>=0.23.4', 
                        'numpy>=1.15.4',
                        'bokeh>=0.13.0',
                        'numba>=0.41.0',
                        'pytest>=4.02'],
      packages=find_packages(exclude=['*tests*',]),
     )