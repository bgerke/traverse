#TraVerSE: Trajectory Veracity for Simple Errors

Python toolkit for assessing veracity of trajectory data, to detect and flag simple errors 
that can be detected based on the properties of each individual trace alone 
(i.e., without making reference to other traces or to map data).

##Installation

It is recommended that TraVerSE be installed and run using Python 3.6 or greater.
The code may also work with earlier versions of Python 3 (and possibly even Python 2.7), 
but this is not formally supported.

###Simple installation
The simplest (though least robust) way to install TraVerSE on
your system is to clone this repository and issue the command `pip3 install -e .` 
\from within the top-level folder of the repository. (It is important to include 
the `-e` flag if you wish to modify any project settings, which you typically will.)

This approach will install the latest version of key dependency packages such as pandas.
Because these may be more recent than the versions under which TraVerSE was originally 
developed, there is no guarantee that TraVerSE will still work properly with these 
more recent versions, though it may, so this approach may be sufficient for trying out
the package.

For more stable installations we recommend installing TraVerSE in a virtual environment, 
as described below.

###Installing and running in a virtual environment
To ensure robustness against future updates to dependency packages, it is a good idea to 
install TraVerSE in a virtual environment and explicitly install the dependency packages 
listed in the `requirements.txt` file.

To do this, first create a virtual environment for TraVerSE following the documentation 
for your favorite tool (common ones include `venv`, `virtualenv`, `pipenv`, and `conda env`). 

After activating your new TraVerSE environment, explicitly install the software 
packages listed in the `requirements.txt` file in the main repository folder (if you 
are using `virtualenv`, for example, you would just invoke `pip3 install -r requirements.txt`). 

Then you can install TraVerSE in your virtual environment by issuing the following command 
from within the main repository folder, with the virtual environment activated:
`pip3 install -e .`. (The `-e` flag is essential if you wish to modify any project settings, 
which is common.)

###Testing
We use the [pytest](docs.pytest.org) package to write and run tests 
for this project. To run all currently existing tests, and make sure the
installation worked, issue the command `$ pytest traverse` at the command line
in the top-level project directory (after activating your virtual environment, 
if appropriate).

##Running the TraVerSE pipeline
Once the package has been installed the user should create a `local.py` 
file in the `traverse/settings` folder, by making a copy of `local-example.py` 
and changing its name to `local.py`. The `local.py` is where one can change various 
settings, such as the time lag thresholds to split a trace into multiple traces. 
The user can set various parameter names to the desired values by uncommenting each 
parameter and changing its value in the `local.py` file. 

An essential first step is to edit the first few parameter names in `local.py` so that 
they match the column names in your dataset corresponding to the timestamp, latitude, 
longitude, and probe ID data. For instance, if your dataset has a column named
`'Probe Latitude'`, you would set `LATFIELD = 'Probe Latitude'` in your local.py
file (note that these field names are treated as *case insensitive* by TraVerSE, 
so `'probe latitude'` would also work).

Once the settings match your data, you can run the entire TraVerSE pipeline by 
navigating to the pipeline folder from your terminal.  From within the pipeline 
folder, at the command line enter 
```
$ python flag_simple_errors.py /path/to/your/input_file.csv [/path/to/your/output_file_name.csv] [thread_count]
``` 

Here `input_file.csv` is your input dataset, which must be in CSV format. 
The output file name and thread count are optional. The thread count is used to parallelize 
the pipeline, which greatly improves the run time of the pipeline. You should set this to the 
number of processing cores you wish to utilize on your system (e.g., on a modern laptop, 
you will probably have at least 4 cores available.) You can check how many cores you have 
in Python with:
```
import multiprocessing
multiprocessing.cpu_count()
```
You can also set a default thread count in your `local.py` file, rather than passing it
at the command line.

The output data set will be a processed data set with errors flagged using the flagging
scheme described below.

##Pipeline Outputs

The output data set from the pipeline contains a suite of new columns. 

All positional and velocity columns are inferred using the lat, long, and timestamp columns. 
The distances and headings are calculated as the crow flies. 


|    Column Type    |  Name         |    Description |
|:-----------------:|:-------------|:---------------|
 |Naming|trace_id|A uuid for each trace.|
|Inferred Analytics|sec_since_prev|The seconds since the previous sample in the trace.|
||dist_from_prev| The (great-circle) distance from the previous sample in the trace.|
||avg_vel_from_prev| The inferred average velocity between the current and previous sample, calculated "as the crow flies".|
||min_avg_vel_from_prev| A lower bound on the average velocity between the current and previous sample, accounting for the timestamp precision. It is recommended that one use this value for a conservative analysis.|
||heading_from_prev|The heading from the previous datapoint to the current one, along a great circle route.|
||sample_period|The period at which a trace reports samples. Missing if no regular sampling period could be determined.|
||simple_head_ch_from_prev|The change in inferred heading that occurs at this point.|
||abs_heading_change_last_2|The absolute change in heading summed over this point and the previous one. This is the relevant metric when looking for position outliers or stationary points.|
| Flagging|error_flag|The error flags that were assigned to each point, encoded as a bit string (see next section for details).|
||stationary_point_flag| A flag to indicate whether a point is part of a stationary point. The object was confined within a restricted radius for a given amount of time.|

##Error Flagging

There are several error flags assigned to the data throughout the pipeline. 
Roughtly speaking, the higher the flag value, the more severe the error. 

The flags assigned are as follows (certain flags have been reserved for possible future
error categories and are not currently used):

|      Category     | Flag |          Error Type          |                                                                                      Description                                                                                     |
|:-----------------:|:----:|:----------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  Anomalous Point |   0  |    Sampling Period  Anomaly     | The trace appears to have a regular sampling period,  but this point is preceded by a time gap that  is not consistent with the sampling Period.                                         |
| |   2  |  Position Anomaly  -- Acute  |   This point appears to be a position outlier  based on the integrated absolute heading change  over the past two points, which indicate  a sudden backtracking or zigzagging pattern. |
| |   3  |       Velocity Outlier       |   Inferred velocity from the previous point is  higher than a reasonable threshold (e.g., 160 kph)                                                                                     |
| Problematic Trace |   5  | Trace with High Anomaly Rate  | Trace has a very high fraction of anomalous points.                                                                                                                                  |
||   6  | Singleton Trace                | Trace consists of only a single point (that does  not have a data error).                                                                                                            |
| Data Error    |   8  | Stuck Location                   | Lat/lon data are exactly equal to the previous  point in the same trace: position data are not  being updated.                                                                       |
||   9  | Duplicate  Timestamps                 | Timestamp is identical to another point in the  same trace. All but one such point is flagged  as a duplicate.                                                                       |
||  10  | Missing Data                          | Data is missing from key fields                                                                                                                                                      |

###Interpreting the error flag values
In the output of the TraVerSE pipeline, the values in the `error_flag` columns are implemented 
as *bit strings*, which use the properties of binary integers to keep track of 
multiple flags in a single piece of data. 

In brief, each error is flagged as a power of 2. For instance, a sampling period anomaly, 
error flag 0, would be flagged as `2**0` or, in binary, `00000001`. Any trace with a sampling 
period anomaly will have a 1 in the last position of its error flag bit. More generally, a point that
received error flag N will have a `1` in the Nth position of the bit, and a point that did not 
receive this flag will have a `0` in that position. 

Using bitwise logic one can easily manipulate and extract information from these error flags. 
Any integer present in the error flag column will be a unique combination of powers of 2. 
Consider the integer 13, for instance. This can be written as `13 = 2**0 + 2**2 +2**3`. 
In binary, 13 is represented as `00001101`. An error flag of 13 therefore would indicate that
a data point has been assigned error flags 0, 2, and 3. 

To test whether a specific data point has received a particular flag, one can use bitwise 
OR logic: If a data point has recieved error flag 3 (a velocity anomaly), for example, 
then its `error_flag` value will cause the following expression to evaluate to `True`: 
`(error_flag & 2**N)>0`. 

If a data point is both a sampling-period anomaly (error flag 0) AND an acute position anomaly
(error flag 2), then the following will evaluate to `True`: `(error_flag & (2**2|2**0))>0`.

If a data point is a sampling-period anomaly *ONLY* (i.e., it received no other error flags),
then it will have `error_flag == 2**0`.


##For developers: A note on notebooks and git
If you are a developer making code commits to this repository, we 
suggest installing the nbstripout package in your local 
repository to keep notebook outputs and metadata from being tracked in 
version control. First install it with `pip install nbstriput`. Then 
enable it by executing the following at the command
line in the top-level directory of the repository:
```
pipenv run nbstripout --install --attributes .gitattributes
```
For more detailed instructions, see the nbstriput documentation here:

https://github.com/kynan/nbstripout

----
*** Copyright Notice ***

"Trajectory Veracity for Simple Errors (TraVerSE)" Copyright (c) 2018, The Regents of the University of California, 
through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals 
from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software, 
please contact Berkeley Lab's Intellectual Property Office at  IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department of Energy and the 
U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted 
for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license 
in the Software to reproduce, distribute copies to the public, prepare derivative works, and 
perform publicly and display publicly, and to permit other to do so. 

